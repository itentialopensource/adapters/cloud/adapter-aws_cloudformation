## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for AWS Cloud Formation. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for AWS Cloud Formation.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the AWS Cloud Formation. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">listStacksSTSRole(nextToken, stackStatusFilter, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the summary information for stacks whose status matches the specified StackStatusFilter.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeStacksSTSRole(stackName, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the description for the specified stack; if no stack name was specified, then it returns the description for all the stacks created.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStackSTSRole(stackName, capabilities, clientRequestToken, disableRollback, enableTerminationProtection, notificationARNs, onFailure, parameters, resourceTypes, roleARN, rollbackConfiguration, stackPolicyBody, stackPolicyURL, tags, templateBody, templateURL, timeoutInMinutes, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a stack as specified in the template.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateStackSTSRole(stackName, capabilities, clientRequestToken, notificationARNs, parameters, resourceTypes, roleARN, rollbackConfiguration, stackPolicyBody, stackPolicyDuringUpdateBody, stackPolicyDuringUpdateURL, stackPolicyURL, tags, templateBody, templateURL, usePreviousTemplate, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates a stack as specified in the template.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTerminationProtectionSTSRole(stackName, enableTerminationProtection, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates termination protection for the specified stack.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStackSTSRole(stackName, clientRequestToken, retainResources, roleARN, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a specified stack.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStackInstancesSTSRole(stackSetName, maxResults, nextToken, stackInstanceAccount, stackInstanceRegion, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns summary information about stack instances that are associated with the specified stack set.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeStackInstanceSTSRole(stackSetName, stackInstanceAccount, stackInstanceRegion, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the stack instance that's associated with the specified stack set, AWS account, and region.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStackInstancesSTSRole(stackSetName, regions, accounts, deploymentTargets, operationId, operationPreferences, parameterOverrides, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates stack instances for the specified accounts, within the specified regions. A stack instance refers to a stack in a specific account and region.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateStackInstancesSTSRole(stackSetName, regions, accounts, deploymentTargets, operationId, operationPreferences, parameterOverrides, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the parameter values for stack instances for the specified accounts, within the specified Regions.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStackInstancesSTSRole(stackSetName, regions, retainStacks, accounts, deploymentTargets, operationId, operationPreferences, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes stack instances for the specified accounts, in the specified regions.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStackSetsSTSRole(maxResults, nextToken, status, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns summary information about stack sets that are associated with the user.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeStackSetSTSRole(stackSetName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the description of the specified stack set.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStackSetSTSRole(stackSetName, administrationRoleARN, autoDeployment, capabilities, clientRequestToken, description, executionRoleName, parameters, permissionModel, tags, templateBody, templateURL, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a stack set.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateStackSetSTSRole(stackSetName, accounts, administrationRoleARN, autoDeployment, capabilities, deploymentTargets, description, executionRoleName, operationId, operationPreferences, parameters, permissionModel, regions, tags, templateBody, templateURL, usePreviousTemplate, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the stack set, and associated stack instances in the specified accounts and Regions.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStackSetSTSRole(stackSetName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a stack set.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelUpdateStackSTSRole(stackName, clientRequestToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Cancels an update on the specified stack.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStackSetOperationsSTSRole(stackSetName, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns summary information about operations performed on a stack set.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStackSetOperationResultsSTSRole(stackSetName, operationId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns summary information about the results of a stack set operation.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeStackSetOperationSTSRole(stackSetName, operationId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the description of the specified stack set operation.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopStackSetOperationSTSRole(stackSetName, operationId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Stops an in-progress operation on a stack set and its associated stack instances.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeStackEventsSTSRole(stackName, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns information about a stack drift detection operation.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStackResourcesSTSRole(stackName, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns descriptions of all resources of the specified stack.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeStackResourceSTSRole(stackName, logicalResourceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns a description of the specified resource in the specified stack.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeStackResourcesSTSRole(stackName, logicalResourceId, physicalResourceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns a description of the specified resource in the specified stack.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeStackResourceDriftsSTSRole(stackName, maxResults, nextToken, stackResourceDriftStatusFilters, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns drift information for the resources that have been checked for drift in the specified stack.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detectStackResourceDriftSTSRole(stackName, logicalResourceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns information about whether a resource's actual configuration differs, or has drifted, from it's expected configuration, as defined in the stack template and any values specified as template parameters.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeStackDriftDetectionStatusSTSRole(stackDriftDetectionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns information about a stack drift detection operation.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detectStackDriftSTSRole(stackName, logicalResourceIds, stsParams, roleName, callback)</td>
    <td style="padding:15px">Detects whether a stack's actual configuration differs, or has drifted, from it's expected configuration, as defined in the stack template and any values specified as template parameters.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detectStackSetDriftSTSRole(stackSetName, operationId, operationPreferences, stsParams, roleName, callback)</td>
    <td style="padding:15px">Detect drift on a stack set.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStackPolicySTSRole(stackName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the stack policy for a specified stack.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setStackPolicySTSRole(stackName, stackPolicyBody, stackPolicyURL, stsParams, roleName, callback)</td>
    <td style="padding:15px">Sets a stack policy for a specified stack.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">continueUpdateRollbackSTSRole(stackName, clientRequestToken, roleARN, resourcesToSkip, stsParams, roleName, callback)</td>
    <td style="padding:15px">For a specified stack that is in the UPDATE_ROLLBACK_FAILED state, continues rolling it back to the UPDATE_ROLLBACK_COMPLETE state</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listChangeSetsSTSRole(stackName, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the ID and status of each active change set for a stack.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeChangeSetSTSRole(changeSetName, stackName, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the inputs for the change set and a list of changes that AWS CloudFormation will make if you execute the change set.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeChangeSetSTSRole(changeSetName, stackName, clientRequestToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates a stack using the input information that was provided when the specified change set was created.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createChangeSetSTSRole(stackName, changeSetName, capabilities, changeSetType, clientToken, description, notificationARNs, parameters, resourcesToImport, resourceTypes, roleARN, rollbackConfiguration, tags, templateBody, templateURL, usePreviousTemplate, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a list of changes that will be applied to a stack so that you can review the changes before executing them.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteChangeSetSTSRole(changeSetName, stackName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified change set. Deleting change sets ensures that no one executes the wrong change set.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTypesSTSRole(deprecatedStatus, maxResults, nextToken, provisioningType, visibility, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns summary information about types that have been registered with CloudFormation.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeTypeSTSRole(arn, type, typeName, versionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns detailed information about a type that has been registered.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTypeRegistrationsSTSRole(maxResults, nextToken, registrationStatusFilter, type, typeArn, typeName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns a list of registration tokens for the specified type(s).</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeTypeRegistrationSTSRole(registrationToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns information about a type's registration, including its current status and type and version identifiers.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerTypeSTSRole(schemaHandlerPackage, clientRequestToken, executionRoleArn, loggingConfig, type, typeName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Registers a type with the CloudFormation service.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deregisterTypeSTSRole(arn, type, typeName, versionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Removes a type or type version from active use in the CloudFormation registry.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTypeVersionsSTSRole(arn, deprecatedStatus, maxResults, nextToken, type, typeName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns summary information about the versions of a type.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setTypeDefaultVersionSTSRole(arn, type, typeName, versionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Specify the default version of a type.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listImportsSTSRole(exportName, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists all stacks that are importing an exported output value.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExportsSTSRole(nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists all exported output values in the account and region in which you call this action.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeAccountLimitsSTSRole(nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Retrieves your account's AWS CloudFormation limits, such as the maximum number of stacks that you can create in your account.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateSTSRole(changeSetName, stackName, templateStage, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the template body for a specified stack.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateSummarySTSRole(stackName, stackSetName, templateBody, templateURL, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns information about a new or existing template.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateTemplateSTSRole(templateBody, templateURL, stsParams, roleName, callback)</td>
    <td style="padding:15px">Validates a specified template.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">estimateTemplateCostSTSRole(parameters, templateBody, templateURL, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the estimated monthly cost of a template.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">signalResourceSTSRole(logicalResourceId, stackName, status, uniqueId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Sends a signal to the specified resource with a success or failure status.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeChangeSetHooksSTSRole(changeSetName, logicalResourceId, nextToken, stackName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns hook-related information for the change set and a list of changes.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateTypeSTSRole(autoUpdate, executionRoleArn, loggingConfig, majorVersion, publicTypeArn, publisherId, type, typeName, typeNameAlias, versionBump, stsParams, roleName, callback)</td>
    <td style="padding:15px">Activates a public third-party extension.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">batchDescribeTypeConfigurationsSTSRole(typeConfigurationIdentifiers, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns configuration data for the specified CloudFormation extensions.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateTypeSTSRole(arn, type, typeName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deactivates a public extension.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publishTypeSTSRole(arn, publicVersionNumber, type, typeName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Publishes the specified extension to the CloudFormation registry.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setTypeConfigurationSTSRole(configuration, configurationAlias, type, typeArn, typeName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Specifies the configuration data for a registered CloudFormation extension.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testTypeSTSRole(arn, logDeliveryBucket, type, typeName, versionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Tests a registered extension.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateOrganizationsAccessSTSRole(stsParams, roleName, callback)</td>
    <td style="padding:15px">Activate trusted access with AWS Organizations</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateOrganizationsAccessSTSRole(stsParams, roleName, callback)</td>
    <td style="padding:15px">Deactivates trusted access with AWS Organizations.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeOrganizationsAccessSTSRole(callAs, stsParams, roleName, callback)</td>
    <td style="padding:15px">Retrieves information about the account's OrganizationAccess status.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importStacksToStackSetSTSRole(callAs, operationId, operationPreferences, organizationalUnitIds, stackIds, stackIdsUrl, stackSetName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Import existing stacks into a new stack sets.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rollbackStackSTSRole(clientRequestToken, roleARN, stackName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Preserve the state of previously provisioned resources when an operation fails.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerPublisherSTSRole(acceptTermsAndConditions, connectionArn, stsParams, roleName, callback)</td>
    <td style="padding:15px">Registers your account as a publisher of public extensions in the CloudFormation registry.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describePublisherSTSRole(publisherId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns information about a CloudFormation extension publisher.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recordHandlerProgressSTSRole(bearerToken, clientRequestToken, currentOperationStatus, errorCode, operationStatus, resourceModel, statusMessage, stsParams, roleName, callback)</td>
    <td style="padding:15px">Reports progress of a resource handler to CloudFormation.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>