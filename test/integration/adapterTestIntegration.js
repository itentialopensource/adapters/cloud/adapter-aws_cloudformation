/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-aws_cloudformation',
      type: 'AwsCloudFormation',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const AwsCloudFormation = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] AwsCloudFormation Adapter Test', () => {
  describe('AwsCloudFormation Class Tests', () => {
    const a = new AwsCloudFormation(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-aws_cloudformation-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-aws_cloudformation-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const stack = 'fakedata';
    describe('#createStack', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createStack(stack, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.CreateStackResponse);
                assert.notEqual(null, data.response.CreateStackResponse.CreateStackResult);
                assert.notEqual(null, data.response.CreateStackResponse.CreateStackResult.StackId);
                assert.equal('arn:aws:cloudformation:us-east-1:123456789:stack/MyStack/aaf549a0-a413-11df-adb3-5081b3858e83', data.response.CreateStackResponse.CreateStackResult.StackId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.CreateStackResponse);
                assert.notEqual(null, data.response.CreateStackResponse.CreateStackResult);
                assert.notEqual(null, data.response.CreateStackResponse.CreateStackResult.StackId);
              }
              saveMockData('Stack', 'createStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listStacks', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listStacks(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListStacksResponse);
                assert.notEqual(null, data.response.ListStacksResponse.ListStacksResult);
                assert.notEqual(null, data.response.ListStacksResponse.ListStacksResult.StackSummaries);
                assert.notEqual(null, data.response.ListStacksResponse.ListStacksResult.StackSummaries.member);
                assert.equal(2, data.response.ListStacksResponse.ListStacksResult.StackSummaries.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListStacksResponse);
                assert.notEqual(null, data.response.ListStacksResponse.ListStacksResult);
                assert.notEqual(null, data.response.ListStacksResponse.ListStacksResult.StackSummaries);
                assert.notEqual(null, data.response.ListStacksResponse.ListStacksResult.StackSummaries.member);
              }
              saveMockData('Stack', 'listStacks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateStack', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateStack(stack, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.UpdateStackResponse);
                assert.notEqual(null, data.response.UpdateStackResponse.UpdateStackResult);
                assert.notEqual(null, data.response.UpdateStackResponse.UpdateStackResult.StackId);
                assert.equal('arn:aws:cloudformation:us-east-1:123456789:stack/MyStack/aaf549a0-a413-11df-adb3-5081b3858e83', data.response.UpdateStackResponse.UpdateStackResult.StackId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.UpdateStackResponse);
                assert.notEqual(null, data.response.UpdateStackResponse.UpdateStackResult);
                assert.notEqual(null, data.response.UpdateStackResponse.UpdateStackResult.StackId);
              }
              saveMockData('Stack', 'updateStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enableTerminationProtection = true;
    describe('#updateTerminationProtection', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTerminationProtection(stack, enableTerminationProtection, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.UpdateTerminationProtectionResponse);
                assert.notEqual(null, data.response.UpdateTerminationProtectionResponse.UpdateTerminationProtectionResult);
                assert.notEqual(null, data.response.UpdateTerminationProtectionResponse.UpdateTerminationProtectionResult.StackId);
                assert.equal('arn:aws:cloudformation:us-east-1:123456789:stack/MyStack/aaf549a0-a413-11df-adb3-5081b3858e83', data.response.UpdateTerminationProtectionResponse.UpdateTerminationProtectionResult.StackId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.UpdateTerminationProtectionResponse);
                assert.notEqual(null, data.response.UpdateTerminationProtectionResponse.UpdateTerminationProtectionResult);
                assert.notEqual(null, data.response.UpdateTerminationProtectionResponse.UpdateTerminationProtectionResult.StackId);
              }
              saveMockData('Stack', 'updateTerminationProtection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeStacks', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeStacks(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStacksResponse);
                assert.notEqual(null, data.response.DescribeStacksResponse.DescribeStacksResult);
                assert.notEqual(null, data.response.DescribeStacksResponse.DescribeStacksResult.Stacks);
                assert.notEqual(null, data.response.DescribeStacksResponse.DescribeStacksResult.Stacks.member);
                assert.notEqual(null, data.response.DescribeStacksResponse.DescribeStacksResult.Stacks.member.StackId);
                assert.equal('arn:aws:cloudformation:us-east-1:123456789:stack/MyStack/aaf549a0-a413-11df-adb3-5081b3858e83', data.response.DescribeStacksResponse.DescribeStacksResult.Stacks.member.StackId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStacksResponse);
                assert.notEqual(null, data.response.DescribeStacksResponse.DescribeStacksResult);
                assert.notEqual(null, data.response.DescribeStacksResponse.DescribeStacksResult.Stacks);
                assert.notEqual(null, data.response.DescribeStacksResponse.DescribeStacksResult.Stacks.member);
                assert.notEqual(null, data.response.DescribeStacksResponse.DescribeStacksResult.Stacks.member.StackId);
              }
              saveMockData('Stack', 'describeStacks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const stackSet = 'fakedata';
    const regions = ['fakedata'];
    describe('#createStackInstances', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createStackInstances(stackSet, regions, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.CreateStackInstancesResponse);
                assert.notEqual(null, data.response.CreateStackInstancesResponse.CreateStackInstancesResult);
                assert.notEqual(null, data.response.CreateStackInstancesResponse.CreateStackInstancesResult.OperationId);
                assert.equal('c424b651-2fda-4d6f-a4f1-20c0fc62a6fe', data.response.CreateStackInstancesResponse.CreateStackInstancesResult.OperationId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.CreateStackInstancesResponse);
                assert.notEqual(null, data.response.CreateStackInstancesResponse.CreateStackInstancesResult);
                assert.notEqual(null, data.response.CreateStackInstancesResponse.CreateStackInstancesResult.OperationId);
              }
              saveMockData('Stack', 'createStackInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listStackInstances', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listStackInstances(stackSet, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListStackInstancesResponse);
                assert.notEqual(null, data.response.ListStackInstancesResponse.ListStackInstancesResult);
                assert.notEqual(null, data.response.ListStackInstancesResponse.ListStackInstancesResult.Summaries);
                assert.notEqual(null, data.response.ListStackInstancesResponse.ListStackInstancesResult.Summaries.member);
                assert.equal(5, data.response.ListStackInstancesResponse.ListStackInstancesResult.Summaries.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListStackInstancesResponse);
                assert.notEqual(null, data.response.ListStackInstancesResponse.ListStackInstancesResult);
                assert.notEqual(null, data.response.ListStackInstancesResponse.ListStackInstancesResult.Summaries);
                assert.notEqual(null, data.response.ListStackInstancesResponse.ListStackInstancesResult.Summaries.member);
              }
              saveMockData('Stack', 'listStackInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateStackInstances', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateStackInstances(stackSet, regions, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.UpdateStackInstancesResponse);
                assert.notEqual(null, data.response.UpdateStackInstancesResponse.UpdateStackInstancesResult);
                assert.notEqual(null, data.response.UpdateStackInstancesResponse.UpdateStackInstancesResult.OperationId);
                assert.equal('c424b651-2fda-4d6f-a4f1-20c0fc62a6fe', data.response.UpdateStackInstancesResponse.UpdateStackInstancesResult.OperationId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.UpdateStackInstancesResponse);
                assert.notEqual(null, data.response.UpdateStackInstancesResponse.UpdateStackInstancesResult);
                assert.notEqual(null, data.response.UpdateStackInstancesResponse.UpdateStackInstancesResult.OperationId);
              }
              saveMockData('Stack', 'updateStackInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const stackInstanceAccount = 'fake data';
    const stackInstanceRegion = 'fake data';
    describe('#describeStackInstance', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeStackInstance(stackSet, stackInstanceAccount, stackInstanceRegion, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackInstanceResponse);
                assert.notEqual(null, data.response.DescribeStackInstanceResponse.DescribeStackInstanceResult);
                assert.notEqual(null, data.response.DescribeStackInstanceResponse.DescribeStackInstanceResult.StackInstance);
                assert.notEqual(null, data.response.DescribeStackInstanceResponse.DescribeStackInstanceResult.StackInstance.StackId);
                assert.equal('arn:aws:cloudformation:ap-northeast-2:012345678910:stack/StackSet-stack-set-example-0ca3eed7-0b67-4be7-8a71-828641fa5193/ea68eca0-f9c1-11e9-aac0-0aaexample', data.response.DescribeStackInstanceResponse.DescribeStackInstanceResult.StackInstance.StackId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackInstanceResponse);
                assert.notEqual(null, data.response.DescribeStackInstanceResponse.DescribeStackInstanceResult);
                assert.notEqual(null, data.response.DescribeStackInstanceResponse.DescribeStackInstanceResult.StackInstance);
                assert.notEqual(null, data.response.DescribeStackInstanceResponse.DescribeStackInstanceResult.StackInstance.StackId);
              }
              saveMockData('Stack', 'describeStackInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createStackSet', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createStackSet(stackSet, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.CreateStackSetResponse);
                assert.notEqual(null, data.response.CreateStackSetResponse.CreateStackSetResult);
                assert.notEqual(null, data.response.CreateStackSetResponse.CreateStackSetResult.StackSetId);
                assert.equal('stack-set-example:22f04391-472b-4e36-b11a-727example', data.response.CreateStackSetResponse.CreateStackSetResult.StackSetId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.CreateStackSetResponse);
                assert.notEqual(null, data.response.CreateStackSetResponse.CreateStackSetResult);
                assert.notEqual(null, data.response.CreateStackSetResponse.CreateStackSetResult.StackSetId);
              }
              saveMockData('Stack', 'createStackSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listStackSets', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listStackSets(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListStackSetsResponse);
                assert.notEqual(null, data.response.ListStackSetsResponse.ListStackSetsResult);
                assert.notEqual(null, data.response.ListStackSetsResponse.ListStackSetsResult.Summaries);
                assert.notEqual(null, data.response.ListStackSetsResponse.ListStackSetsResult.Summaries.member);
                assert.equal(2, data.response.ListStackSetsResponse.ListStackSetsResult.Summaries.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListStackSetsResponse);
                assert.notEqual(null, data.response.ListStackSetsResponse.ListStackSetsResult);
                assert.notEqual(null, data.response.ListStackSetsResponse.ListStackSetsResult.Summaries);
                assert.notEqual(null, data.response.ListStackSetsResponse.ListStackSetsResult.Summaries.member);
              }
              saveMockData('Stack', 'listStackSets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateStackSet', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateStackSet(stackSet, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.UpdateStackSetResponse);
                assert.notEqual(null, data.response.UpdateStackSetResponse.UpdateStackSetResult);
                assert.notEqual(null, data.response.UpdateStackSetResponse.UpdateStackSetResult.OperationId);
                assert.equal('bb1764f4-3dea-4c39-bd65-066aexamplef', data.response.UpdateStackSetResponse.UpdateStackSetResult.OperationId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.CreateStackSetResponse);
                assert.notEqual(null, data.response.CreateStackSetResponse.CreateStackSetResult);
                assert.notEqual(null, data.response.CreateStackSetResponse.CreateStackSetResult.StackSetId);
              }
              saveMockData('Stack', 'updateStackSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeStackSet', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeStackSet(stackSet, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackSetResponse);
                assert.notEqual(null, data.response.DescribeStackSetResponse.DescribeStackSetResult);
                assert.notEqual(null, data.response.DescribeStackSetResponse.DescribeStackSetResult.StackSet);
                assert.notEqual(null, data.response.DescribeStackSetResponse.DescribeStackSetResult.StackSet.StackSetId);
                assert.equal('stack-set-example:c14cd6d1-cd17-40bd-82ed-ff97example', data.response.DescribeStackSetResponse.DescribeStackSetResult.StackSet.StackSetId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackSetResponse);
                assert.notEqual(null, data.response.DescribeStackSetResponse.DescribeStackSetResult);
                assert.notEqual(null, data.response.DescribeStackSetResponse.DescribeStackSetResult.StackSet);
                assert.notEqual(null, data.response.DescribeStackSetResponse.DescribeStackSetResult.StackSet.StackSetId);
              }
              saveMockData('Stack', 'describeStackSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listStackSetOperations', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listStackSetOperations(stackSet, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListStackSetOperationsResponse);
                assert.notEqual(null, data.response.ListStackSetOperationsResponse.ListStackSetOperationsResult);
                assert.notEqual(null, data.response.ListStackSetOperationsResponse.ListStackSetOperationsResult.Summaries);
                assert.notEqual(null, data.response.ListStackSetOperationsResponse.ListStackSetOperationsResult.Summaries.member);
                assert.equal(3, data.response.ListStackSetOperationsResponse.ListStackSetOperationsResult.Summaries.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListStackSetOperationsResponse);
                assert.notEqual(null, data.response.ListStackSetOperationsResponse.ListStackSetOperationsResult);
                assert.notEqual(null, data.response.ListStackSetOperationsResponse.ListStackSetOperationsResult.Summaries);
                assert.notEqual(null, data.response.ListStackSetOperationsResponse.ListStackSetOperationsResult.Summaries.member);
              }
              saveMockData('Stack', 'listStackSetOperations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const operationId = 'fake data';
    describe('#listStackSetOperationResults', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listStackSetOperationResults(stackSet, operationId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListStackSetOperationResultsResponse);
                assert.notEqual(null, data.response.ListStackSetOperationResultsResponse.ListStackSetOperationResultsResult);
                assert.notEqual(null, data.response.ListStackSetOperationResultsResponse.ListStackSetOperationResultsResult.Summaries);
                assert.notEqual(null, data.response.ListStackSetOperationResultsResponse.ListStackSetOperationResultsResult.Summaries.member);
                assert.equal(2, data.response.ListStackSetOperationResultsResponse.ListStackSetOperationResultsResult.Summaries.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListStackSetOperationResultsResponse);
                assert.notEqual(null, data.response.ListStackSetOperationResultsResponse.ListStackSetOperationResultsResult);
                assert.notEqual(null, data.response.ListStackSetOperationResultsResponse.ListStackSetOperationResultsResult.Summaries);
                assert.notEqual(null, data.response.ListStackSetOperationResultsResponse.ListStackSetOperationResultsResult.Summaries.member);
              }
              saveMockData('Stack', 'listStackSetOperationResults', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeStackSetOperation', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeStackSetOperation(stackSet, operationId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackSetOperationResponse);
                assert.notEqual(null, data.response.DescribeStackSetOperationResponse.DescribeStackSetOperationResult);
                assert.notEqual(null, data.response.DescribeStackSetOperationResponse.DescribeStackSetOperationResult.StackSetOperation);
                assert.notEqual(null, data.response.DescribeStackSetOperationResponse.DescribeStackSetOperationResult.StackSetOperation.StackSetId);
                assert.equal('stack-set-drift-example:bd1f4017-d4f9-432e-a73f-8c22eb708dd5', data.response.DescribeStackSetOperationResponse.DescribeStackSetOperationResult.StackSetOperation.StackSetId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackSetOperationResponse);
                assert.notEqual(null, data.response.DescribeStackSetOperationResponse.DescribeStackSetOperationResult);
                assert.notEqual(null, data.response.DescribeStackSetOperationResponse.DescribeStackSetOperationResult.StackSetOperation);
                assert.notEqual(null, data.response.DescribeStackSetOperationResponse.DescribeStackSetOperationResult.StackSetOperation.StackSetId);
              }
              saveMockData('Stack', 'describeStackSetOperation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeStackEvents', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeStackEvents(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackEventsResponse);
                assert.notEqual(null, data.response.DescribeStackEventsResponse.DescribeStackEventsResult);
                assert.notEqual(null, data.response.DescribeStackEventsResponse.DescribeStackEventsResult.StackEvents);
                assert.notEqual(null, data.response.DescribeStackEventsResponse.DescribeStackEventsResult.StackEvents.member);
                assert.equal(5, data.response.DescribeStackEventsResponse.DescribeStackEventsResult.StackEvents.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackEventsResponse);
                assert.notEqual(null, data.response.DescribeStackEventsResponse.DescribeStackEventsResult);
                assert.notEqual(null, data.response.DescribeStackEventsResponse.DescribeStackEventsResult.StackEvents);
                assert.notEqual(null, data.response.DescribeStackEventsResponse.DescribeStackEventsResult.StackEvents.member);
              }
              saveMockData('Stack', 'describeStackEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listStackResources', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listStackResources(stack, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListStackResourcesResponse);
                assert.notEqual(null, data.response.ListStackResourcesResponse.ListStackResourcesResult);
                assert.notEqual(null, data.response.ListStackResourcesResponse.ListStackResourcesResult.StackResourceSummaries);
                assert.notEqual(null, data.response.ListStackResourcesResponse.ListStackResourcesResult.StackResourceSummaries.member);
                assert.equal(6, data.response.ListStackResourcesResponse.ListStackResourcesResult.StackResourceSummaries.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListStackResourcesResponse);
                assert.notEqual(null, data.response.ListStackResourcesResponse.ListStackResourcesResult);
                assert.notEqual(null, data.response.ListStackResourcesResponse.ListStackResourcesResult.StackResourceSummaries);
                assert.notEqual(null, data.response.ListStackResourcesResponse.ListStackResourcesResult.StackResourceSummaries.member);
              }
              saveMockData('Stack', 'listStackResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const logicalResourceId = 'fake data';
    describe('#describeStackResource', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeStackResource(stack, logicalResourceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackResourceResponse);
                assert.notEqual(null, data.response.DescribeStackResourceResponse.DescribeStackResourceResult);
                assert.notEqual(null, data.response.DescribeStackResourceResponse.DescribeStackResourceResult.StackResourceDetail);
                assert.notEqual(null, data.response.DescribeStackResourceResponse.DescribeStackResourceResult.StackResourceDetail.StackId);
                assert.equal('arn:aws:cloudformation:us-east-1:123456789:stack/MyStack/aaf549a0-a413-11df-adb3-5081b3858e83', data.response.DescribeStackResourceResponse.DescribeStackResourceResult.StackResourceDetail.StackId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackResourceResponse);
                assert.notEqual(null, data.response.DescribeStackResourceResponse.DescribeStackResourceResult);
                assert.notEqual(null, data.response.DescribeStackResourceResponse.DescribeStackResourceResult.StackResourceDetail);
                assert.notEqual(null, data.response.DescribeStackResourceResponse.DescribeStackResourceResult.StackResourceDetail.StackId);
              }
              saveMockData('Stack', 'describeStackResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeStackResources', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeStackResources(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackResourcesResponse);
                assert.notEqual(null, data.response.DescribeStackResourcesResponse.DescribeStackResourcesResult);
                assert.notEqual(null, data.response.DescribeStackResourcesResponse.DescribeStackResourcesResult.StackResources);
                assert.notEqual(null, data.response.DescribeStackResourcesResponse.DescribeStackResourcesResult.StackResources.member);
                assert.equal(2, data.response.DescribeStackResourcesResponse.DescribeStackResourcesResult.StackResources.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackResourcesResponse);
                assert.notEqual(null, data.response.DescribeStackResourcesResponse.DescribeStackResourcesResult);
                assert.notEqual(null, data.response.DescribeStackResourcesResponse.DescribeStackResourcesResult.StackResources);
                assert.notEqual(null, data.response.DescribeStackResourcesResponse.DescribeStackResourcesResult.StackResources.member);
              }
              saveMockData('Stack', 'describeStackResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeStackResourceDrifts', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeStackResourceDrifts(stack, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackResourceDriftsResponse);
                assert.notEqual(null, data.response.DescribeStackResourceDriftsResponse.DescribeStackResourceDriftsResult);
                assert.notEqual(null, data.response.DescribeStackResourceDriftsResponse.DescribeStackResourceDriftsResult.StackResourceDrifts);
                assert.notEqual(null, data.response.DescribeStackResourceDriftsResponse.DescribeStackResourceDriftsResult.StackResourceDrifts.member);
                assert.notEqual(null, data.response.DescribeStackResourceDriftsResponse.DescribeStackResourceDriftsResult.StackResourceDrifts.member.StackId);
                assert.equal('arn:aws:cloudformation:us-east-1:012345678910:stack/my-stack-with-resource-drift/489e5570-df85-11e7-a7d9-503acac5c0fd', data.response.DescribeStackResourceDriftsResponse.DescribeStackResourceDriftsResult.StackResourceDrifts.member.StackId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackResourceDriftsResponse);
                assert.notEqual(null, data.response.DescribeStackResourceDriftsResponse.DescribeStackResourceDriftsResult);
                assert.notEqual(null, data.response.DescribeStackResourceDriftsResponse.DescribeStackResourceDriftsResult.StackResourceDrifts);
                assert.notEqual(null, data.response.DescribeStackResourceDriftsResponse.DescribeStackResourceDriftsResult.StackResourceDrifts.member);
                assert.notEqual(null, data.response.DescribeStackResourceDriftsResponse.DescribeStackResourceDriftsResult.StackResourceDrifts.member.StackId);
              }
              saveMockData('Stack', 'describeStackResourceDrifts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detectStackResourceDrift', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.detectStackResourceDrift(stack, logicalResourceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DetectStackResourceDriftResponse);
                assert.notEqual(null, data.response.DetectStackResourceDriftResponse.DetectStackResourceDriftResult);
                assert.notEqual(null, data.response.DetectStackResourceDriftResponse.DetectStackResourceDriftResult.StackResourceDrift);
                assert.notEqual(null, data.response.DetectStackResourceDriftResponse.DetectStackResourceDriftResult.StackResourceDrift.StackId);
                assert.equal('arn:aws:cloudformation:us-east-1:012345678910:stack/my-stack-with-resource-drift/489e5570-df85-11e7-a7d9-503acac5c0fd', data.response.DetectStackResourceDriftResponse.DetectStackResourceDriftResult.StackResourceDrift.StackId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DetectStackResourceDriftResponse);
                assert.notEqual(null, data.response.DetectStackResourceDriftResponse.DetectStackResourceDriftResult);
                assert.notEqual(null, data.response.DetectStackResourceDriftResponse.DetectStackResourceDriftResult.StackResourceDrift);
                assert.notEqual(null, data.response.DetectStackResourceDriftResponse.DetectStackResourceDriftResult.StackResourceDrift.StackId);
              }
              saveMockData('Stack', 'detectStackResourceDrift', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const stackDriftDetectionId = 1;
    describe('#describeStackDriftDetectionStatus', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeStackDriftDetectionStatus(stackDriftDetectionId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackDriftDetectionStatusResponse);
                assert.notEqual(null, data.response.DescribeStackDriftDetectionStatusResponse.DescribeStackDriftDetectionStatusResult);
                assert.notEqual(null, data.response.DescribeStackDriftDetectionStatusResponse.DescribeStackDriftDetectionStatusResult.StackId);
                assert.equal('arn:aws:cloudformation:us-east-1:012345678910:stack/example/cb438120-6cc7-11e7-998e-50example', data.response.DescribeStackDriftDetectionStatusResponse.DescribeStackDriftDetectionStatusResult.StackId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeStackDriftDetectionStatusResponse);
                assert.notEqual(null, data.response.DescribeStackDriftDetectionStatusResponse.DescribeStackDriftDetectionStatusResult);
                assert.notEqual(null, data.response.DescribeStackDriftDetectionStatusResponse.DescribeStackDriftDetectionStatusResult.StackId);
              }
              saveMockData('Stack', 'describeStackDriftDetectionStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detectStackDrift', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.detectStackDrift(stack, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DetectStackDriftResponse);
                assert.notEqual(null, data.response.DetectStackDriftResponse.DetectStackDriftResult);
                assert.notEqual(null, data.response.DetectStackDriftResponse.DetectStackDriftResult.StackDriftDetectionId);
                assert.equal('2f2b2d60-df86-11e7-bea1-500c2example', data.response.DetectStackDriftResponse.DetectStackDriftResult.StackDriftDetectionId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DetectStackDriftResponse);
                assert.notEqual(null, data.response.DetectStackDriftResponse.DetectStackDriftResult);
                assert.notEqual(null, data.response.DetectStackDriftResponse.DetectStackDriftResult.StackDriftDetectionId);
              }
              saveMockData('Stack', 'detectStackDrift', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detectStackSetDrift', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.detectStackSetDrift(stackSet, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DetectStackSetDriftResponse);
                assert.notEqual(null, data.response.DetectStackSetDriftResponse.DetectStackSetDriftResult);
                assert.notEqual(null, data.response.DetectStackSetDriftResponse.DetectStackSetDriftResult.OperationId);
                assert.equal('9cc082fa-df4c-45cd-b9a8-7e56example', data.response.DetectStackSetDriftResponse.DetectStackSetDriftResult.OperationId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DetectStackSetDriftResponse);
                assert.notEqual(null, data.response.DetectStackSetDriftResponse.DetectStackSetDriftResult);
                assert.notEqual(null, data.response.DetectStackSetDriftResponse.DetectStackSetDriftResult.OperationId);
              }
              saveMockData('Stack', 'detectStackSetDrift', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setStackPolicy', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.setStackPolicy(stack, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Stack', 'setStackPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStackPolicy', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStackPolicy(stack, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.GetStackPolicyResponse);
                assert.notEqual(null, data.response.GetStackPolicyResponse.GetStackPolicyResult);
                assert.notEqual(null, data.response.GetStackPolicyResponse.GetStackPolicyResult.StackPolicyBody);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.GetStackPolicyResponse);
                assert.notEqual(null, data.response.GetStackPolicyResponse.GetStackPolicyResult);
                assert.notEqual(null, data.response.GetStackPolicyResponse.GetStackPolicyResult.StackPolicyBody);
              }
              saveMockData('Stack', 'getStackPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#continueUpdateRollback', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.continueUpdateRollback(stack, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ContinueUpdateRollbackResponse);
                assert.notEqual(null, data.response.ContinueUpdateRollbackResponse.ResponseMetadata);
                assert.notEqual(null, data.response.ContinueUpdateRollbackResponse.ResponseMetadata.RequestId);
                assert.equal('5ccc7dcd-744c-11e5-be70-1b08c228efb3', data.response.ContinueUpdateRollbackResponse.ResponseMetadata.RequestId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ContinueUpdateRollbackResponse);
                assert.notEqual(null, data.response.ContinueUpdateRollbackResponse.ResponseMetadata);
                assert.notEqual(null, data.response.ContinueUpdateRollbackResponse.ResponseMetadata.RequestId);
              }
              saveMockData('Rollback', 'continueUpdateRollback', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeSet = 'fakedata';
    describe('#createChangeSet', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createChangeSet(stack, changeSet, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.CreateChangeSetResponse);
                assert.notEqual(null, data.response.CreateChangeSetResponse.CreateChangeSetResult);
                assert.notEqual(null, data.response.CreateChangeSetResponse.CreateChangeSetResult.Id);
                assert.equal('arn:aws:cloudformation:us-east-1:123456789012:changeSet/SampleChangeSet/12a3b456-0e10-4ce0-9052-5d484a8c4e5b', data.response.CreateChangeSetResponse.CreateChangeSetResult.Id);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.CreateChangeSetResponse);
                assert.notEqual(null, data.response.CreateChangeSetResponse.CreateChangeSetResult);
                assert.notEqual(null, data.response.CreateChangeSetResponse.CreateChangeSetResult.Id);
              }
              saveMockData('ChangeSet', 'createChangeSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listChangeSets', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listChangeSets(stack, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListChangeSetsResponse);
                assert.notEqual(null, data.response.ListChangeSetsResponse.ListChangeSetsResult);
                assert.notEqual(null, data.response.ListChangeSetsResponse.ListChangeSetsResult.Summaries);
                assert.notEqual(null, data.response.ListChangeSetsResponse.ListChangeSetsResult.Summaries.member);
                assert.equal(3, data.response.ListChangeSetsResponse.ListChangeSetsResult.Summaries.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListChangeSetsResponse);
                assert.notEqual(null, data.response.ListChangeSetsResponse.ListChangeSetsResult);
                assert.notEqual(null, data.response.ListChangeSetsResponse.ListChangeSetsResult.Summaries);
                assert.notEqual(null, data.response.ListChangeSetsResponse.ListChangeSetsResult.Summaries.member);
              }
              saveMockData('ChangeSet', 'listChangeSets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeChangeSet', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeChangeSet(changeSet, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeChangeSetResponse);
                assert.notEqual(null, data.response.DescribeChangeSetResponse.DescribeChangeSetResult);
                assert.notEqual(null, data.response.DescribeChangeSetResponse.DescribeChangeSetResult.StackId);
                assert.equal('arn:aws:cloudformation:us-east-1:123456789012:stack/SampleStack/12a3b456-0e10-4ce0-9052-5d484a8c4e5b', data.response.DescribeChangeSetResponse.DescribeChangeSetResult.StackId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeChangeSetResponse);
                assert.notEqual(null, data.response.DescribeChangeSetResponse.DescribeChangeSetResult);
                assert.notEqual(null, data.response.DescribeChangeSetResponse.DescribeChangeSetResult.StackId);
              }
              saveMockData('ChangeSet', 'describeChangeSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#executeChangeSet', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.executeChangeSet(changeSet, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('ChangeSet', 'executeChangeSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schemaHandlerPackage = 'fake data';
    describe('#registerType', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.registerType(schemaHandlerPackage, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.RegisterTypeResponse);
                assert.notEqual(null, data.response.RegisterTypeResponse.RegisterTypeResult);
                assert.notEqual(null, data.response.RegisterTypeResponse.RegisterTypeResult.RegistrationToken);
                assert.equal('f5525280-104e-4d35-bef5-8f1f1example', data.response.RegisterTypeResponse.RegisterTypeResult.RegistrationToken);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Type', 'registerType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTypes', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTypes(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListTypesResponse);
                assert.notEqual(null, data.response.ListTypesResponse.ListTypesResult);
                assert.notEqual(null, data.response.ListTypesResponse.ListTypesResult.TypeSummaries);
                assert.notEqual(null, data.response.ListTypesResponse.ListTypesResult.TypeSummaries.member);
                assert.equal(2, data.response.ListTypesResponse.ListTypesResult.TypeSummaries.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListTypesResponse);
                assert.notEqual(null, data.response.ListTypesResponse.ListTypesResult);
                assert.notEqual(null, data.response.ListTypesResponse.ListTypesResult.TypeSummaries);
                assert.notEqual(null, data.response.ListTypesResponse.ListTypesResult.TypeSummaries.member);
              }
              saveMockData('Type', 'listTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeType', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeType(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeTypeResponse);
                assert.notEqual(null, data.response.DescribeTypeResponse.DescribeTypeResult);
                assert.notEqual(null, data.response.DescribeTypeResponse.DescribeTypeResult.Type);
                assert.equal('RESOURCE', data.response.DescribeTypeResponse.DescribeTypeResult.Type);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeTypeResponse);
                assert.notEqual(null, data.response.DescribeTypeResponse.DescribeTypeResult);
                assert.notEqual(null, data.response.DescribeTypeResponse.DescribeTypeResult.Type);
              }
              saveMockData('Type', 'describeType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTypeRegistrations', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTypeRegistrations(null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListTypeRegistrationsResponse);
                assert.notEqual(null, data.response.ListTypeRegistrationsResponse.ListTypeRegistrationsResult);
                assert.notEqual(null, data.response.ListTypeRegistrationsResponse.ListTypeRegistrationsResult.RegistrationTokenList);
                assert.notEqual(null, data.response.ListTypeRegistrationsResponse.ListTypeRegistrationsResult.RegistrationTokenList.member);
                assert.equal(3, data.response.ListTypeRegistrationsResponse.ListTypeRegistrationsResult.RegistrationTokenList.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListTypeRegistrationsResponse);
                assert.notEqual(null, data.response.ListTypeRegistrationsResponse.ListTypeRegistrationsResult);
                assert.notEqual(null, data.response.ListTypeRegistrationsResponse.ListTypeRegistrationsResult.RegistrationTokenList);
                assert.notEqual(null, data.response.ListTypeRegistrationsResponse.ListTypeRegistrationsResult.RegistrationTokenList.member);
              }
              saveMockData('Type', 'listTypeRegistrations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const registrationToken = 'fake data';
    describe('#describeTypeRegistration', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeTypeRegistration(registrationToken, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeTypeRegistrationResponse);
                assert.notEqual(null, data.response.DescribeTypeRegistrationResponse.DescribeTypeRegistrationResult);
                assert.notEqual(null, data.response.DescribeTypeRegistrationResponse.DescribeTypeRegistrationResult.ProgressStatus);
                assert.equal('IN_PROGRESS', data.response.DescribeTypeRegistrationResponse.DescribeTypeRegistrationResult.ProgressStatus);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeTypeRegistrationResponse);
                assert.notEqual(null, data.response.DescribeTypeRegistrationResponse.DescribeTypeRegistrationResult);
                assert.notEqual(null, data.response.DescribeTypeRegistrationResponse.DescribeTypeRegistrationResult.ProgressStatus);
              }
              saveMockData('Type', 'describeTypeRegistration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setTypeDefaultVersion', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.setTypeDefaultVersion(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Type', 'setTypeDefaultVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTypeVersions', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTypeVersions(null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListTypeVersionsResponse);
                assert.notEqual(null, data.response.ListTypeVersionsResponse.ListTypeVersionsResult);
                assert.notEqual(null, data.response.ListTypeVersionsResponse.ListTypeVersionsResult.TypeVersionSummaries);
                assert.notEqual(null, data.response.ListTypeVersionsResponse.ListTypeVersionsResult.TypeVersionSummaries.member);
                assert.equal(2, data.response.ListTypeVersionsResponse.ListTypeVersionsResult.TypeVersionSummaries.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListTypeRegistrationsResponse);
                assert.notEqual(null, data.response.ListTypeRegistrationsResponse.ListTypeRegistrationsResult);
                assert.notEqual(null, data.response.ListTypeRegistrationsResponse.ListTypeRegistrationsResult.RegistrationTokenList);
                assert.notEqual(null, data.response.ListTypeRegistrationsResponse.ListTypeRegistrationsResult.RegistrationTokenList.member);
              }
              saveMockData('Type', 'listTypeVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exportName = 'fake data';
    describe('#listImports', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listImports(exportName, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListImportsResponse);
                assert.notEqual(null, data.response.ListImportsResponse.ListImportsResult);
                assert.notEqual(null, data.response.ListImportsResponse.ListImportsResult.Imports);
                assert.notEqual(null, data.response.ListImportsResponse.ListImportsResult.Imports.member);
                assert.equal('Import-SampleStack', data.response.ListImportsResponse.ListImportsResult.Imports.member);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListImportsResponse);
                assert.notEqual(null, data.response.ListImportsResponse.ListImportsResult);
                assert.notEqual(null, data.response.ListImportsResponse.ListImportsResult.Imports);
                assert.notEqual(null, data.response.ListImportsResponse.ListImportsResult.Imports.member);
              }
              saveMockData('Account', 'listImports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExports', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listExports(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListExportsResponse);
                assert.notEqual(null, data.response.ListExportsResponse.ListExportsResult);
                assert.notEqual(null, data.response.ListExportsResponse.ListExportsResult.Exports);
                assert.notEqual(null, data.response.ListExportsResponse.ListExportsResult.Exports.member);
                assert.equal(4, data.response.ListExportsResponse.ListExportsResult.Exports.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ListExportsResponse);
                assert.notEqual(null, data.response.ListExportsResponse.ListExportsResult);
                assert.notEqual(null, data.response.ListExportsResponse.ListExportsResult.Exports);
                assert.notEqual(null, data.response.ListExportsResponse.ListExportsResult.Exports.member);
              }
              saveMockData('Account', 'listExports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeAccountLimits', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeAccountLimits(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeAccountLimitsResponse);
                assert.notEqual(null, data.response.DescribeAccountLimitsResponse.DescribeAccountLimitsResult);
                assert.notEqual(null, data.response.DescribeAccountLimitsResponse.DescribeAccountLimitsResult.AccountLimits);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.DescribeAccountLimitsResponse);
                assert.notEqual(null, data.response.DescribeAccountLimitsResponse.DescribeAccountLimitsResult);
                assert.notEqual(null, data.response.DescribeAccountLimitsResponse.DescribeAccountLimitsResult.AccountLimits);
              }
              saveMockData('Account', 'describeAccountLimits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplate', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplate(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.GetTemplateResponse);
                assert.notEqual(null, data.response.GetTemplateResponse.GetTemplateResult);
                assert.notEqual(null, data.response.GetTemplateResponse.GetTemplateResult.TemplateBody);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.GetTemplateResponse);
                assert.notEqual(null, data.response.GetTemplateResponse.GetTemplateResult);
                assert.notEqual(null, data.response.GetTemplateResponse.GetTemplateResult.TemplateBody);
              }
              saveMockData('Template', 'getTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateSummary', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateSummary(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.GetTemplateSummaryResponse);
                assert.notEqual(null, data.response.GetTemplateSummaryResponse.GetTemplateSummaryResult);
                assert.notEqual(null, data.response.GetTemplateSummaryResponse.GetTemplateSummaryResult.Description);
                assert.equal('A sample template description.', data.response.GetTemplateSummaryResponse.GetTemplateSummaryResult.Description);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.GetTemplateSummaryResponse);
                assert.notEqual(null, data.response.GetTemplateSummaryResponse.GetTemplateSummaryResult);
                assert.notEqual(null, data.response.GetTemplateSummaryResponse.GetTemplateSummaryResult.Description);
              }
              saveMockData('Template', 'getTemplateSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateTemplate', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.validateTemplate(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ValidateTemplateResponse);
                assert.notEqual(null, data.response.ValidateTemplateResponse.ValidateTemplateResult);
                assert.notEqual(null, data.response.ValidateTemplateResponse.ValidateTemplateResult.Parameters);
                assert.notEqual(null, data.response.ValidateTemplateResponse.ValidateTemplateResult.Parameters.member);
                assert.equal(3, data.response.ValidateTemplateResponse.ValidateTemplateResult.Parameters.member.length);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.ValidateTemplateResponse);
                assert.notEqual(null, data.response.ValidateTemplateResponse.ValidateTemplateResult);
                assert.notEqual(null, data.response.ValidateTemplateResponse.ValidateTemplateResult.Parameters);
                assert.notEqual(null, data.response.ValidateTemplateResponse.ValidateTemplateResult.Parameters.member);
              }
              saveMockData('Template', 'validateTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#estimateTemplateCost', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.estimateTemplateCost(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.Response);
                assert.notEqual(null, data.response.Response.EstimateTemplateCostResult);
                assert.notEqual(null, data.response.Response.EstimateTemplateCostResult.Url);
                assert.equal('http://calculator.s3.amazonaws.com/calc5.html?key=cf-2e351785-e821-450c-9d58-625e1e1ebfb6', data.response.Response.EstimateTemplateCostResult.Url);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.Response);
                assert.notEqual(null, data.response.Response.EstimateTemplateCostResult);
                assert.notEqual(null, data.response.Response.EstimateTemplateCostResult.Url);
              }
              saveMockData('Template', 'estimateTemplateCost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const status = 'fake data';
    const uniqueId = 'fake data';
    describe('#signalResource', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.signalResource(logicalResourceId, stack, status, uniqueId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Resource', 'signalResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deregisterType', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deregisterType(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Type', 'deregisterType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelUpdateStack', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelUpdateStack(stack, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.CancelUpdateStackResponse);
                assert.notEqual(null, data.response.CancelUpdateStackResponse.ResponseMetadata);
                assert.notEqual(null, data.response.CancelUpdateStackResponse.ResponseMetadata.RequestId);
                assert.equal('5ccc7dcd-744c-11e5-be70-1b08c228efb3', data.response.CancelUpdateStackResponse.ResponseMetadata.RequestId);
              } else {
                assert.equal('object', typeof data.response);
                assert.notEqual(null, data.response.CancelUpdateStackResponse);
                assert.notEqual(null, data.response.CancelUpdateStackResponse.ResponseMetadata);
                assert.notEqual(null, data.response.CancelUpdateStackResponse.ResponseMetadata.RequestId);
              }
              saveMockData('Stack', 'cancelUpdateStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stopStackSetOperation', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.stopStackSetOperation(stackSet, operationId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Stack', 'stopStackSetOperation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteChangeSet', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteChangeSet(changeSet, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('ChangeSet', 'deleteChangeSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const retainStacks = 'fake data';
    describe('#deleteStackInstances', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteStackInstances(stackSet, regions, retainStacks, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Stack', 'deleteStackInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStackSet', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteStackSet(stackSet, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Stack', 'deleteStackSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStack', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteStack(stack, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Stack', 'deleteStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateOrganizationsAccess', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.activateOrganizationsAccess((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Organization', 'activateOrganizationsAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateType', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.activateType(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Type', 'activateType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const typeConfigurationIdentifiers = [{ fakedata: 'fakedata' }];
    describe('#batchDescribeTypeConfigurations', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.batchDescribeTypeConfigurations(typeConfigurationIdentifiers, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Type', 'batchDescribeTypeConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateOrganizationsAccess', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deactivateOrganizationsAccess((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Organization', 'deactivateOrganizationsAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateType', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deactivateType(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Type', 'deactivateType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeSetName = 'fakedata';
    describe('#describeChangeSetHooks', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeChangeSetHooks(changeSetName, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('ChangeSet', 'describeChangeSetHooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeOrganizationsAccess', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeOrganizationsAccess(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Organization', 'describeOrganizationsAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describePublisher', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describePublisher(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Account', 'describePublisher', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const stackSetName = 'fakedata';
    describe('#importStacksToStackSet', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importStacksToStackSet(null, null, null, null, null, null, stackSetName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Stack', 'importStacksToStackSet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publishType', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.publishType(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Type', 'publishType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bearerToken = 'fakedata';
    const operationStatus = 'fakedata';
    describe('#recordHandlerProgress', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.recordHandlerProgress(bearerToken, null, null, null, operationStatus, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Resource', 'recordHandlerProgress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#registerPublisher', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.registerPublisher(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Account', 'registerPublisher', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const stackName = 'fakedata';
    describe('#rollbackStack', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rollbackStack(null, null, stackName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Stack', 'rollbackStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configuration = 'fakedata';
    describe('#setTypeConfiguration', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.setTypeConfiguration(configuration, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Type', 'setTypeConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testType', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.testType(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('Type', 'testType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
