/* @copyright Itential, LLC 2019 (pre-modifications) */

/* eslint import/no-dynamic-require: warn */
/* eslint object-curly-newline: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint camelcase: warn  */
/* eslint no-param-reassign: warn  */

// Set globals
/* global log */

/* Required libraries.  */
const path = require('path');

const callOptions = {
  service: 'cloudformation',
  datatype: {
    request: 'URLENCODE',
    response: 'XML2JSON'
  }
};
const authMethod = 'aws_authentication';

/* Fetch in the other needed components for the this Adaptor */
const AdapterBaseCl = require(path.join(__dirname, 'adapterBase.js'));

/*
 * INTERNAL FUNCTION: return stub results based on the path and method
 */
function processInArray(inArray, currentParams, key) {
  if (!inArray || !Array.isArray(inArray) || inArray.length === 0) {
    return;
  }

  // Go through the array to convert the items in it
  for (let i = 0; i < inArray.length; i += 1) {
    // are we dealing with just data
    if (typeof inArray[i] === 'string' || typeof inArray[i] === 'number' || typeof inArray[i] === 'boolean') {
      currentParams[`${key}.member.${i + 1}`] = inArray[i];
    } else if (typeof inArray[i] === 'object') {
      // got an object - need to add keys and values
      const objKeys = Object.keys(inArray[i]);

      for (let o = 0; o < objKeys.length; o += 1) {
        const thisKey = objKeys[o]; // Assume we get from data with proper casing
        const thisValue = inArray[i][objKeys[o]];

        if (Array.isArray(thisValue)) {
          processInArray(thisValue, currentParams, `${key}.member.${i + 1}.${thisKey}`);
        } else {
          currentParams[`${key}.member.${i + 1}.${thisKey}`] = thisValue;
        }
      }
    }
  }
}

/**
 * This is the adapter/interface into AwsCloudFormation
 */

/* GENERAL ADAPTER FUNCTIONS */
class AwsCloudFormation extends AdapterBaseCl {
  /**
   * AwsCloudFormation Adapter
   * @constructor
   */
  constructor(prongid, properties) {
    // make sure properties are set - so that we do not break
    const myProperties = properties;

    // service should exist and be ec2
    if (myProperties && !myProperties.service) {
      myProperties.service = callOptions.service;
    }
    // auth_method should now be aws_authentication
    if (myProperties && myProperties.authentication && (!myProperties.authentication.auth_method
       || myProperties.authentication.auth_method === 'no_authentication')) {
      myProperties.authentication.auth_method = authMethod;
    }

    // Instantiate the AdapterBase super class
    super(prongid, myProperties);

    /*
    const restFunctionNames = this.getWorkflowFunctions();

    // Dynamically bind emit functions
    for (let i = 0; i < restFunctionNames.length; i += 1) {
      // Bind function to have name fnNameEmit for fnName
      const version = restFunctionNames[i].match(/__v[0-9]+/);
      const baseFnName = restFunctionNames[i].replace(/__v[0-9]+/, '');
      const fnNameEmit = version ? `${baseFnName}Emit${version}` : `${baseFnName}Emit`;
      this[fnNameEmit] = function (...args) {
        // extract the callback
        const callback = args[args.length - 1];
        // slice the callback from args so we can insert our own
        const functionArgs = args.slice(0, args.length - 1);
        // create a random name for the listener
        const eventName = `${restFunctionNames[i]}:${Math.random().toString(36)}`;
        // tell the calling class to start listening
        callback({ event: eventName, status: 'received' });
        // store parent for use of this context later
        const parent = this;
        // store emission function
        const func = function (val, err) {
          parent.removeListener(eventName, func);
          parent.emit(eventName, val, err);
        };
        // Use apply to call the function in a specific context
        this[restFunctionNames[i]].apply(this, functionArgs.concat([func])); // eslint-disable-line prefer-spread
      };
    }

    // Uncomment if you have things to add to the constructor like using your own properties.
    // Otherwise the constructor in the adapterBase will be used.
    // Capture my own properties - they need to be defined in propertiesSchema.json
    // if (this.allProps && this.allProps.myownproperty) {
    //   mypropvariable = this.allProps.myownproperty;
    // }
    */
  }

  /**
   * @callback healthCallback
   * @param {Object} reqObj - the request to send into the healthcheck
   * @param {Callback} callback - The results of the call
   */
  healthCheck(reqObj, callback) {
    // you can modify what is passed into the healthcheck by changing things in the newReq
    let newReq = null;
    if (reqObj) {
      newReq = Object.assign(...reqObj);
    }
    super.healthCheck(newReq, callback);
  }

  /**
     * @iapGetAdapterWorkflowFunctions
     */
  iapGetAdapterWorkflowFunctions(inIgnore) {
    let myIgnore = [
      'healthCheck',
      'iapGetAdapterWorkflowFunctions',
      'hasEntities',
      'getAuthorization'
    ];
    if (!inIgnore && Array.isArray(inIgnore)) {
      myIgnore = inIgnore;
    } else if (!inIgnore && typeof inIgnore === 'string') {
      myIgnore = [inIgnore];
    }

    // The generic adapter functions should already be ignored (e.g. healthCheck)
    // you can add specific methods that you do not want to be workflow functions to ignore like below
    // myIgnore.push('myMethodNotInWorkflow');

    return super.iapGetAdapterWorkflowFunctions(myIgnore);
  }

  /**
     * iapUpdateAdapterConfiguration is used to update any of the adapter configuration files. This
     * allows customers to make changes to adapter configuration without having to be on the
     * file system.
     *
     * @function iapUpdateAdapterConfiguration
     * @param {string} configFile - the name of the file being updated (required)
     * @param {Object} changes - an object containing all of the changes = formatted like the configuration file (required)
     * @param {string} entity - the entity to be changed, if an action, schema or mock data file (optional)
     * @param {string} type - the type of entity file to change, (action, schema, mock) (optional)
     * @param {string} action - the action to be changed, if an action, schema or mock data file (optional)
     * @param {boolean} replace - true to replace entire mock data, false to merge/append
     * @param {Callback} callback - The results of the call
     */
  iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, replace, callback) {
    const meth = 'adapter-iapUpdateAdapterConfiguration';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    super.iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, replace, callback);
  }

  /**
      * @summary Suspends adapter
      *
      * @function iapSuspendAdapter
      * @param {Callback} callback - callback function
      */
  iapSuspendAdapter(mode, callback) {
    const meth = 'adapter-iapSuspendAdapter';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapSuspendAdapter(mode, callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
      * @summary Unsuspends adapter
      *
      * @function iapUnsuspendAdapter
      * @param {Callback} callback - callback function
      */
  iapUnsuspendAdapter(callback) {
    const meth = 'adapter-iapUnsuspendAdapter';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapUnsuspendAdapter(callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
      * @summary Get the Adapter Queue
      *
      * @function iapGetAdapterQueue
      * @param {Callback} callback - callback function
      */
  iapGetAdapterQueue(callback) {
    const meth = 'adapter-iapGetAdapterQueue';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    return super.iapGetAdapterQueue(callback);
  }

  /* SCRIPT CALLS */
  /**
     * See if the API path provided is found in this adapter
     *
     * @function iapFindAdapterPath
     * @param {string} apiPath - the api path to check on
     * @param {Callback} callback - The results of the call
     */
  iapFindAdapterPath(apiPath, callback) {
    const meth = 'adapter-iapFindAdapterPath';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    super.iapFindAdapterPath(apiPath, callback);
  }

  /**
    * @summary Runs troubleshoot scripts for adapter
    *
    * @function iapTroubleshootAdapter
    * @param {Object} props - the connection, healthcheck and authentication properties
    *
    * @param {boolean} persistFlag - whether the adapter properties should be updated
    * @param {Callback} callback - The results of the call
    */
  iapTroubleshootAdapter(props, persistFlag, callback) {
    const meth = 'adapter-iapTroubleshootAdapter';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapTroubleshootAdapter(props, persistFlag, this, callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
      * @summary runs healthcheck script for adapter
      *
      * @function iapRunAdapterHealthcheck
      * @param {Adapter} adapter - adapter instance to troubleshoot
      * @param {Callback} callback - callback function
      */
  iapRunAdapterHealthcheck(callback) {
    const meth = 'adapter-iapRunAdapterHealthcheck';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapRunAdapterHealthcheck(this, callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
      * @summary runs connectivity check script for adapter
      *
      * @function iapRunAdapterConnectivity
      * @param {Callback} callback - callback function
      */
  iapRunAdapterConnectivity(callback) {
    const meth = 'adapter-iapRunAdapterConnectivity';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapRunAdapterConnectivity(callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
      * @summary runs basicGet script for adapter
      *
      * @function iapRunAdapterBasicGet
      * @param {Callback} callback - callback function
      */
  iapRunAdapterBasicGet(callback) {
    const meth = 'adapter-iapRunAdapterBasicGet';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapRunAdapterBasicGet(callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
     * @summary moves entites into Mongo DB
     *
     * @function iapMoveAdapterEntitiesToDB
     * @param {getCallback} callback - a callback function to return the result (Generics)
     *                                  or the error
     */
  iapMoveAdapterEntitiesToDB(callback) {
    const meth = 'adapter-iapMoveAdapterEntitiesToDB';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapMoveAdapterEntitiesToDB(callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
     * @summary Deactivate adapter tasks
     *
     * @function iapDeactivateTasks
     *
     * @param {Array} tasks - List of tasks to deactivate
     * @param {Callback} callback
     */
  iapDeactivateTasks(tasks, callback) {
    const meth = 'adapter-iapDeactivateTasks';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapDeactivateTasks(tasks, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
     * @summary Activate adapter tasks that have previously been deactivated
     *
     * @function iapActivateTasks
     *
     * @param {Array} tasks - List of tasks to activate
     * @param {Callback} callback
     */
  iapActivateTasks(tasks, callback) {
    const meth = 'adapter-iapActivateTasks';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapActivateTasks(tasks, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /* CACHE CALLS */
  /**
     * @summary Populate the cache for the given entities
     *
     * @function iapPopulateEntityCache
     * @param {String/Array of Strings} entityType - the entity type(s) to populate
     * @param {Callback} callback - whether the cache was updated or not for each entity type
     *
     * @returns status of the populate
     */
  iapPopulateEntityCache(entityTypes, callback) {
    const meth = 'adapter-iapPopulateEntityCache';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapPopulateEntityCache(entityTypes, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
     * @summary Retrieves data from cache for specified entity type
     *
     * @function iapRetrieveEntitiesCache
     * @param {String} entityType - entity of which to retrieve
     * @param {Object} options - settings of which data to return and how to return it
     * @param {Callback} callback - the data if it was retrieved
     */
  iapRetrieveEntitiesCache(entityType, options, callback) {
    const meth = 'adapter-iapCheckEiapRetrieveEntitiesCachentityCached';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapRetrieveEntitiesCache(entityType, options, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /* BROKER CALLS */
  /**
   * @summary Determines if this adapter supports any in a list of entities
   *
   * @function hasEntities
   * @param {String} entityType - the entity type to check for
   * @param {Array} entityList - the list of entities we are looking for
   *
   * @param {Callback} callback - A map where the entity is the key and the
   *                              value is true or false
   */
  hasEntities(entityType, entityList, callback) {
    const meth = 'adapter-hasEntities';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.hasEntitiesAuth(entityType, entityList, callOptions, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Get Appliance that match the deviceName
   *
   * @function getDevice
   * @param {String} deviceName - the deviceName to find (required)
   *
   * @param {getCallback} callback - a callback function to return the result
   *                                 (appliance) or the error
   */
  getDevice(deviceName, callback) {
    const meth = 'adapter-getDevice';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.getDeviceAuth(deviceName, callOptions, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Get Appliances that match the filter
   *
   * @function getDevicesFiltered
   * @param {Object} options - the data to use to filter the appliances (optional)
   *
   * @param {getCallback} callback - a callback function to return the result
   *                                 (appliances) or the error
   */
  getDevicesFiltered(options, callback) {
    const meth = 'adapter-getDevicesFiltered';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // if no filtering
      if (!options || !options.filter || !options.filter.name) {
        return super.getDevicesFilteredAuth({}, callOptions, callback);
      }

      // Need to do filtering here because AWS does not support filtering with contains
      return super.getDevicesFilteredAuth({}, callOptions, (devices, error) => {
        if (error) {
          return callback(null, error);
        }

        if (devices.list) {
          const filterDevices = [];
          for (let i = 0; i < devices.list.length; i += 1) {
            if (devices.list[i].name.includes(options.filter.name)) {
              filterDevices.push(devices.list[i]);
            }
          }
          return callback({ total: filterDevices.length, list: filterDevices });
        }

        return devices;
      });
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Gets the status for the provided appliance
   *
   * @function isAlive
   * @param {String} deviceName - the deviceName of the appliance. (required)
   *
   * @param {configCallback} callback - callback function to return the result
   *                                    (appliance isAlive) or the error
   */
  isAlive(deviceName, callback) {
    const meth = 'adapter-isAlive';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.isAliveAuth(deviceName, callOptions, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Gets a config for the provided Appliance
   *
   * @function getConfig
   * @param {String} deviceName - the deviceName of the appliance. (required)
   * @param {String} format - the desired format of the config. (optional)
   *
   * @param {configCallback} callback - callback function to return the result
   *                                    (appliance config) or the error
   */
  getConfig(deviceName, format, callback) {
    const meth = 'adapter-getConfig';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.getConfigAuth(deviceName, format, callOptions, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Gets the device count from the system
   *
   * @function iapGetDeviceCount
   *
   * @param {getCallback} callback - callback function to return the result
   *                                    (count) or the error
   */
  iapGetDeviceCount(callback) {
    const meth = 'adapter-iapGetDeviceCount';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapGetDeviceCountAuth(callOptions, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /* GENERIC ADAPTER REQUEST - allows extension of adapter without new calls being added */
  /**
     * Makes the requested generic call
     *
     * @function iapExpandedGenericAdapterRequest
     * @param {Object} metadata - metadata for the call (optional).
     *                 Can be a stringified Object.
     * @param {String} uriPath - the path of the api call - do not include the host, port, base path or version (optional)
     * @param {String} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (optional)
     * @param {Object} pathVars - the parameters to be put within the url path (optional).
     *                 Can be a stringified Object.
     * @param {Object} queryData - the parameters to be put on the url (optional).
     *                 Can be a stringified Object.
     * @param {Object} requestBody - the body to add to the request (optional).
     *                 Can be a stringified Object.
     * @param {Object} addlHeaders - additional headers to be put on the call (optional).
     *                 Can be a stringified Object.
     * @param {getCallback} callback - a callback function to return the result (Generics)
     *                 or the error
     */
  iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback) {
    const meth = 'adapter-iapExpandedGenericAdapterRequest';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
     * Makes the requested generic call
     *
     * @function genericAdapterRequest
     * @param {String} uriPath - the path of the api call - do not include the host, port, base path or version (required)
     * @param {String} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (required)
     * @param {Object} queryData - the parameters to be put on the url (optional).
     *                 Can be a stringified Object.
     * @param {Object} requestBody - the body to add to the request (optional).
     *                 Can be a stringified Object.
     * @param {Object} addlHeaders - additional headers to be put on the call (optional).
     *                 Can be a stringified Object.
     * @param {getCallback} callback - a callback function to return the result (Generics)
     *                 or the error
     */
  genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback) {
    const meth = 'adapter-genericAdapterRequest';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
     * Makes the requested generic call with no base path or version
     *
     * @function genericAdapterRequestNoBasePath
     * @param {String} uriPath - the path of the api call - do not include the host, port, base path or version (required)
     * @param {String} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (required)
     * @param {Object} queryData - the parameters to be put on the url (optional).
     *                 Can be a stringified Object.
     * @param {Object} requestBody - the body to add to the request (optional).
     *                 Can be a stringified Object.
     * @param {Object} addlHeaders - additional headers to be put on the call (optional).
     *                 Can be a stringified Object.
     * @param {getCallback} callback - a callback function to return the result (Generics)
     *                 or the error
     */
  genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback) {
    const meth = 'adapter-genericAdapterRequestNoBasePath';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /* INVENTORY CALLS */
  /**
     * @summary run the adapter lint script to return the results.
     *
     * @function iapRunAdapterLint
     * @param {Callback} callback - callback function
     */
  iapRunAdapterLint(callback) {
    const meth = 'adapter-iapRunAdapterLint';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    return super.iapRunAdapterLint(callback);
  }

  /**
     * @summary run the adapter test scripts (baseunit and unit) to return the results.
     *    can not run integration as there can be implications with that.
     *
     * @function iapRunAdapterTests
     * @param {Callback} callback - callback function
     */
  iapRunAdapterTests(callback) {
    const meth = 'adapter-iapRunAdapterTests';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    return super.iapRunAdapterTests(callback);
  }

  /**
     * @summary provide inventory information abbout the adapter
     *
     * @function iapGetAdapterInventory
     * @param {Callback} callback - callback function
     */
  iapGetAdapterInventory(callback) {
    const meth = 'adapter-iapGetAdapterInventory';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    return super.iapGetAdapterInventory(callback);
  }

  /**
   * @callback healthCallback
   * @param {Object} result - the result of the get request (contains an id and a status)
   */
  /**
   * @callback getCallback
   * @param {Object} result - the result of the get request (entity/ies)
   * @param {String} error - any error that occurred
   */
  /**
   * @callback createCallback
   * @param {Object} item - the newly created entity
   * @param {String} error - any error that occurred
   */
  /**
   * @callback updateCallback
   * @param {String} status - the status of the update action
   * @param {String} error - any error that occurred
   */
  /**
   * @callback deleteCallback
   * @param {String} status - the status of the delete action
   * @param {String} error - any error that occurred
   */

  /**
   * @summary Returns the summary information for stacks whose status matches the specified StackStatusFilter.
   *
   * @function listStacks
   * @param {string} nextToken - A string that identifies the next page of stacks that you want to retrieve.
   * @param {array} stackStatusFilter - The name or unique ID of the stack set whose description you want.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listStacks(nextToken, stackStatusFilter, callback) {
    this.listStacksSTSRole(nextToken, stackStatusFilter, null, null, callback);
  }

  /**
   * @summary Returns the summary information for stacks whose status matches the specified StackStatusFilter.
   *
   * @function listStacksSTSRole
   * @param {string} nextToken - A string that identifies the next page of stacks that you want to retrieve.
   * @param {array} stackStatusFilter - The name or unique ID of the stack set whose description you want.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listStacksSTSRole(nextToken, stackStatusFilter, stsParams, roleName, callback) {
    const meth = 'adapter-listStacksSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ListStacks';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      NextToken: nextToken,
      Action: action,
      Version: version
    };
    if (stackStatusFilter && stackStatusFilter.length > 0) {
      processInArray(stackStatusFilter, queryParamsAvailable, 'StackStatusFilter');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'listStacks', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['listStacksSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns the description for the specified stack; if no stack name was specified, then it returns the description for all the stacks created.
   *
   * @function describeStacks
   * @param {string} stackName - The name or unique ID of the stack set whose description you want.
   * @param {string} nextToken - A string that identifies the next page of stacks that you want to retrieve.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStacks(stackName, nextToken, callback) {
    this.describeStacksSTSRole(stackName, nextToken, null, null, callback);
  }

  /**
   * @summary Returns the description for the specified stack; if no stack name was specified, then it returns the description for all the stacks created.
   *
   * @function describeStacksSTSRole
   * @param {string} stackName - The name or unique ID of the stack set whose description you want.
   * @param {string} nextToken - A string that identifies the next page of stacks that you want to retrieve.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStacksSTSRole(stackName, nextToken, stsParams, roleName, callback) {
    const meth = 'adapter-describeStacksSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeStacks';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      NextToken: nextToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'describeStacks', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeStacksSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Creates a stack as specified in the template.
   *
   * @function createStack
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {array} capabilities - In some cases, you must explicitly acknowledge that your stack template contains certain capabilities in order for AWS CloudFormation to create the stack.
   * @param {string} clientRequestToken - A unique identifier for this CreateStack request.
   * @param {boolean} disableRollback - Set to true to disable rollback of the stack if stack creation failed.
   * @param {boolean} enableTerminationProtection - Set to true to disable rollback of the stack if stack creation failed.
   * @param {array} notificationARNs - The Simple Notification Service (SNS) topic ARNs to publish stack related events.
   * @param {string} onFailure - Determines what action will be taken if stack creation fails.
   * @param {array} parameters - A list of Parameter structures that specify input parameters for the stack.
   * @param {array} resourceTypes - The template resource types that you have permissions to work with for this create stack action.
   * @param {string} roleARN - The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that AWS CloudFormation assumes to roll back the stack.
   * @param {object} rollbackConfiguration - The rollback triggers for AWS CloudFormation to monitor during stack creation and updating operations, and for the specified monitoring period afterwards.
   * @param {string} stackPolicyBody - Structure containing the stack policy body.
   * @param {string} stackPolicyURL - Location of a file containing the stack policy.
   * @param {array} tags - Key-value pairs to associate with this stack.
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {number} timeoutInMinutes - The amount of time that can pass before the stack status becomes CREATE_FAILED;.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createStack(stackName, capabilities, clientRequestToken, disableRollback, enableTerminationProtection, notificationARNs, onFailure, parameters, resourceTypes, roleARN, rollbackConfiguration, stackPolicyBody, stackPolicyURL, tags, templateBody, templateURL, timeoutInMinutes, callback) {
    this.createStackSTSRole(stackName, capabilities, clientRequestToken, disableRollback, enableTerminationProtection, notificationARNs, onFailure, parameters, resourceTypes, roleARN, rollbackConfiguration, stackPolicyBody, stackPolicyURL, tags, templateBody, templateURL, timeoutInMinutes, null, null, callback);
  }

  /**
   * @summary Creates a stack as specified in the template.
   *
   * @function createStackSTSRole
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {array} capabilities - In some cases, you must explicitly acknowledge that your stack template contains certain capabilities in order for AWS CloudFormation to create the stack.
   * @param {string} clientRequestToken - A unique identifier for this CreateStack request.
   * @param {boolean} disableRollback - Set to true to disable rollback of the stack if stack creation failed.
   * @param {boolean} enableTerminationProtection - Set to true to disable rollback of the stack if stack creation failed.
   * @param {array} notificationARNs - The Simple Notification Service (SNS) topic ARNs to publish stack related events.
   * @param {string} onFailure - Determines what action will be taken if stack creation fails.
   * @param {array} parameters - A list of Parameter structures that specify input parameters for the stack.
   * @param {array} resourceTypes - The template resource types that you have permissions to work with for this create stack action.
   * @param {string} roleARN - The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that AWS CloudFormation assumes to roll back the stack.
   * @param {object} rollbackConfiguration - The rollback triggers for AWS CloudFormation to monitor during stack creation and updating operations, and for the specified monitoring period afterwards.
   * @param {string} stackPolicyBody - Structure containing the stack policy body.
   * @param {string} stackPolicyURL - Location of a file containing the stack policy.
   * @param {array} tags - Key-value pairs to associate with this stack.
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {number} timeoutInMinutes - The amount of time that can pass before the stack status becomes CREATE_FAILED;.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createStackSTSRole(stackName, capabilities, clientRequestToken, disableRollback, enableTerminationProtection, notificationARNs, onFailure, parameters, resourceTypes, roleARN, rollbackConfiguration, stackPolicyBody, stackPolicyURL, tags, templateBody, templateURL, timeoutInMinutes, stsParams, roleName, callback) {
    const meth = 'adapter-createStackSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'CreateStack';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      ClientRequestToken: clientRequestToken,
      DisableRollback: disableRollback,
      EnableTerminationProtection: enableTerminationProtection,
      OnFailure: onFailure,
      RoleARN: roleARN,
      RollbackConfiguration: rollbackConfiguration,
      StackPolicyBody: stackPolicyBody,
      StackPolicyURL: stackPolicyURL,
      TemplateBody: templateBody,
      TemplateURL: templateURL,
      TimeoutInMinutes: timeoutInMinutes,
      Action: action,
      Version: version
    };
    if (capabilities && capabilities.length > 0) {
      processInArray(capabilities, queryParamsAvailable, 'Capabilities');
    }
    if (notificationARNs && notificationARNs.length > 0) {
      processInArray(notificationARNs, queryParamsAvailable, 'NotificationARNs');
    }
    if (parameters && parameters.length > 0) {
      processInArray(parameters, queryParamsAvailable, 'Parameters');
    }
    if (resourceTypes && resourceTypes.length > 0) {
      processInArray(resourceTypes, queryParamsAvailable, 'ResourceTypes');
    }
    if (tags && tags.length > 0) {
      processInArray(tags, queryParamsAvailable, 'Tags');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'createStack', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createStackSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Updates a stack as specified in the template.
   *
   * @function updateStack
   * @param {string} stackName - The name or unique stack ID of the stack to update.
   * @param {array} capabilities - In some cases, you must explicitly acknowledge that your stack template contains certain capabilities in order for AWS CloudFormation to update the stack.
   * @param {string} clientRequestToken - A unique identifier for this UpdateStack request.
   * @param {array} notificationARNs - Amazon Simple Notification Service topic Amazon Resource Names (ARNs) that AWS CloudFormation associates with the stack.
   * @param {array} parameters - A list of Parameter structures that specify input parameters for the stack.
   * @param {array} resourceTypes - The template resource types that you have permissions to work with for this update stack action.
   * @param {string} roleARN - The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that AWS CloudFormation assumes to update the stack.
   * @param {object} rollbackConfiguration - The rollback triggers for AWS CloudFormation to monitor during stack creation and updating operations, and for the specified monitoring period afterwards.
   * @param {string} stackPolicyBody - Structure containing a new stack policy body.
   * @param {string} stackPolicyDuringUpdateBody - Structure containing the temporary overriding stack policy body.
   * @param {string} stackPolicyDuringUpdateURL - Location of a file containing the temporary overriding stack policy.
   * @param {string} stackPolicyURL - Location of a file containing the updated stack policy.
   * @param {array} tags - Key-value pairs to associate with this stack.
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {boolean} usePreviousTemplate - Reuse the existing template that is associated with the stack that you are updating.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateStack(stackName, capabilities, clientRequestToken, notificationARNs, parameters, resourceTypes, roleARN, rollbackConfiguration, stackPolicyBody, stackPolicyDuringUpdateBody, stackPolicyDuringUpdateURL, stackPolicyURL, tags, templateBody, templateURL, usePreviousTemplate, callback) {
    this.updateStackSTSRole(stackName, capabilities, clientRequestToken, notificationARNs, parameters, resourceTypes, roleARN, rollbackConfiguration, stackPolicyBody, stackPolicyDuringUpdateBody, stackPolicyDuringUpdateURL, stackPolicyURL, tags, templateBody, templateURL, usePreviousTemplate, null, null, callback);
  }

  /**
   * @summary Updates a stack as specified in the template.
   *
   * @function updateStackSTSRole
   * @param {string} stackName - The name or unique stack ID of the stack to update.
   * @param {array} capabilities - In some cases, you must explicitly acknowledge that your stack template contains certain capabilities in order for AWS CloudFormation to update the stack.
   * @param {string} clientRequestToken - A unique identifier for this UpdateStack request.
   * @param {array} notificationARNs - Amazon Simple Notification Service topic Amazon Resource Names (ARNs) that AWS CloudFormation associates with the stack.
   * @param {array} parameters - A list of Parameter structures that specify input parameters for the stack.
   * @param {array} resourceTypes - The template resource types that you have permissions to work with for this update stack action.
   * @param {string} roleARN - The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that AWS CloudFormation assumes to update the stack.
   * @param {object} rollbackConfiguration - The rollback triggers for AWS CloudFormation to monitor during stack creation and updating operations, and for the specified monitoring period afterwards.
   * @param {string} stackPolicyBody - Structure containing a new stack policy body.
   * @param {string} stackPolicyDuringUpdateBody - Structure containing the temporary overriding stack policy body.
   * @param {string} stackPolicyDuringUpdateURL - Location of a file containing the temporary overriding stack policy.
   * @param {string} stackPolicyURL - Location of a file containing the updated stack policy.
   * @param {array} tags - Key-value pairs to associate with this stack.
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {boolean} usePreviousTemplate - Reuse the existing template that is associated with the stack that you are updating.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateStackSTSRole(stackName, capabilities, clientRequestToken, notificationARNs, parameters, resourceTypes, roleARN, rollbackConfiguration, stackPolicyBody, stackPolicyDuringUpdateBody, stackPolicyDuringUpdateURL, stackPolicyURL, tags, templateBody, templateURL, usePreviousTemplate, stsParams, roleName, callback) {
    const meth = 'adapter-updateStackSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'UpdateStack';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      ClientRequestToken: clientRequestToken,
      RoleARN: roleARN,
      RollbackConfiguration: rollbackConfiguration,
      StackPolicyBody: stackPolicyBody,
      StackPolicyDuringUpdateBody: stackPolicyDuringUpdateBody,
      StackPolicyDuringUpdateURL: stackPolicyDuringUpdateURL,
      StackPolicyURL: stackPolicyURL,
      TemplateBody: templateBody,
      TemplateURL: templateURL,
      UsePreviousTemplate: usePreviousTemplate,
      Action: action,
      Version: version
    };
    if (capabilities && capabilities.length > 0) {
      processInArray(capabilities, queryParamsAvailable, 'Capabilities');
    }
    if (notificationARNs && notificationARNs.length > 0) {
      processInArray(notificationARNs, queryParamsAvailable, 'NotificationARNs');
    }
    if (parameters && parameters.length > 0) {
      processInArray(parameters, queryParamsAvailable, 'Parameters');
    }
    if (resourceTypes && resourceTypes.length > 0) {
      processInArray(resourceTypes, queryParamsAvailable, 'ResourceTypes');
    }
    if (tags && tags.length > 0) {
      processInArray(tags, queryParamsAvailable, 'Tags');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'updateStack', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateStackSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Updates termination protection for the specified stack.
   *
   * @function updateTerminationProtection
   * @param {string} stackName - The name or unique ID of the stack for which you want to set termination protection.
   * @param {boolean} enableTerminationProtection - Whether to enable termination protection on the specified stack.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateTerminationProtection(stackName, enableTerminationProtection, callback) {
    this.updateTerminationProtectionSTSRole(stackName, enableTerminationProtection, null, null, callback);
  }

  /**
   * @summary Updates termination protection for the specified stack.
   *
   * @function updateTerminationProtectionSTSRole
   * @param {string} stackName - The name or unique ID of the stack for which you want to set termination protection.
   * @param {boolean} enableTerminationProtection - Whether to enable termination protection on the specified stack.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateTerminationProtectionSTSRole(stackName, enableTerminationProtection, stsParams, roleName, callback) {
    const meth = 'adapter-updateTerminationProtectionSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'UpdateTerminationProtection';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (enableTerminationProtection === undefined || enableTerminationProtection === null || enableTerminationProtection === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['enableTerminationProtection'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      EnableTerminationProtection: enableTerminationProtection,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'updateTerminationProtection', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateTerminationProtectionSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Deletes a specified stack.
   *
   * @function deleteStack
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {string} clientRequestToken - A unique identifier for this DeleteStack request.
   * @param {array} retainResources - For stacks in the DELETE_FAILED state, a list of resource logical IDs that are associated with the resources you want to retain.
   * @param {string} roleARN - The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that AWS CloudFormation assumes to delete the stack.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteStack(stackName, clientRequestToken, retainResources, roleARN, callback) {
    this.deleteStackSTSRole(stackName, clientRequestToken, retainResources, roleARN, null, null, callback);
  }

  /**
   * @summary Deletes a specified stack.
   *
   * @function deleteStackSTSRole
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {string} clientRequestToken - A unique identifier for this DeleteStack request.
   * @param {array} retainResources - For stacks in the DELETE_FAILED state, a list of resource logical IDs that are associated with the resources you want to retain.
   * @param {string} roleARN - The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that AWS CloudFormation assumes to delete the stack.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteStackSTSRole(stackName, clientRequestToken, retainResources, roleARN, stsParams, roleName, callback) {
    const meth = 'adapter-deleteStackSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DeleteStack';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      ClientRequestToken: clientRequestToken,
      RoleARN: roleARN,
      Action: action,
      Version: version
    };
    if (retainResources && retainResources.length > 0) {
      processInArray(retainResources, queryParamsAvailable, 'RetainResources');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'deleteStack', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteStackSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns summary information about stack instances that are associated with the specified stack set.
   *
   * @function listStackInstances
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to list stack instances for
   * @param {string} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - The name of the AWS account that you want to list stack instances for.
   * @param {string} stackInstanceAccount - The name of the AWS account that you want to list stack instances for.
   * @param {string} stackInstanceRegion - The name of the region where you want to list stack instances.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listStackInstances(stackSetName, maxResults, nextToken, stackInstanceAccount, stackInstanceRegion, callback) {
    this.listStackInstancesSTSRole(stackSetName, maxResults, nextToken, stackInstanceAccount, stackInstanceRegion, null, null, callback);
  }

  /**
   * @summary Returns summary information about stack instances that are associated with the specified stack set.
   *
   * @function listStackInstancesSTSRole
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to list stack instances for
   * @param {string} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - The name of the AWS account that you want to list stack instances for.
   * @param {string} stackInstanceAccount - The name of the AWS account that you want to list stack instances for.
   * @param {string} stackInstanceRegion - The name of the region where you want to list stack instances.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listStackInstancesSTSRole(stackSetName, maxResults, nextToken, stackInstanceAccount, stackInstanceRegion, stsParams, roleName, callback) {
    const meth = 'adapter-listStackInstancesSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ListStackInstances';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      MaxResults: maxResults,
      NextToken: nextToken,
      StackInstanceAccount: stackInstanceAccount,
      StackInstanceRegion: stackInstanceRegion,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'listStackInstances', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['listStackInstancesSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns the stack instance that's associated with the specified stack set, AWS account, and region.
   *
   * @function describeStackInstance
   * @param {string} stackSetName - The name or the unique stack ID of the stack set that you want to get stack instance information for.
   * @param {string} stackInstanceAccount - The ID of an AWS account that's associated with this stack instance.
   * @param {string} stackInstanceRegion - The name of a region that's associated with this stack instance.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackInstance(stackSetName, stackInstanceAccount, stackInstanceRegion, callback) {
    this.describeStackInstanceSTSRole(stackSetName, stackInstanceAccount, stackInstanceRegion, null, null, callback);
  }

  /**
   * @summary Returns the stack instance that's associated with the specified stack set, AWS account, and region.
   *
   * @function describeStackInstanceSTSRole
   * @param {string} stackSetName - The name or the unique stack ID of the stack set that you want to get stack instance information for.
   * @param {string} stackInstanceAccount - The ID of an AWS account that's associated with this stack instance.
   * @param {string} stackInstanceRegion - The name of a region that's associated with this stack instance.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackInstanceSTSRole(stackSetName, stackInstanceAccount, stackInstanceRegion, stsParams, roleName, callback) {
    const meth = 'adapter-describeStackInstanceSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeStackInstance';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (stackInstanceAccount === undefined || stackInstanceAccount === null || stackInstanceAccount === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackInstanceAccount'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (stackInstanceRegion === undefined || stackInstanceRegion === null || stackInstanceRegion === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackInstanceRegion'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      StackInstanceAccount: stackInstanceAccount,
      StackInstanceRegion: stackInstanceRegion,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'describeStackInstance', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeStackInstanceSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Creates stack instances for the specified accounts, within the specified regions.
   *
   * @function createStackInstances
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to create stack instances from.
   * @param {array} regions - The names of one or more regions where you want to create stack instances using the specified AWS account(s).
   * @param {array} accounts - The names of one or more AWS accounts that you want to create stack instances in the specified region(s) for.
   * @param {object} deploymentTargets - The AWS Organizations accounts for which to create stack instances in the specified Regions.
   * @param {string} operationId - The unique identifier for this stack set operation.
   * @param {object} operationPreferences - Preferences for how AWS CloudFormation performs this stack set operation.
   * @param {array} parameterOverrides - A list of stack set parameters whose values you want to override in the selected stack instances.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createStackInstances(stackSetName, regions, accounts, deploymentTargets, operationId, operationPreferences, parameterOverrides, callback) {
    this.createStackInstancesSTSRole(stackSetName, regions, accounts, deploymentTargets, operationId, operationPreferences, parameterOverrides, null, null, callback);
  }

  /**
   * @summary Creates stack instances for the specified accounts, within the specified regions.
   *
   * @function createStackInstancesSTSRole
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to create stack instances from.
   * @param {array} regions - The names of one or more regions where you want to create stack instances using the specified AWS account(s).
   * @param {array} accounts - The names of one or more AWS accounts that you want to create stack instances in the specified region(s) for.
   * @param {object} deploymentTargets - The AWS Organizations accounts for which to create stack instances in the specified Regions.
   * @param {string} operationId - The unique identifier for this stack set operation.
   * @param {object} operationPreferences - Preferences for how AWS CloudFormation performs this stack set operation.
   * @param {array} parameterOverrides - A list of stack set parameters whose values you want to override in the selected stack instances.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createStackInstancesSTSRole(stackSetName, regions, accounts, deploymentTargets, operationId, operationPreferences, parameterOverrides, stsParams, roleName, callback) {
    const meth = 'adapter-createStackInstancesSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'CreateStackInstances';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (regions === undefined || regions === null || !Array.isArray(regions) || regions.length < 1) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['regions'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      DeploymentTargets: deploymentTargets,
      OperationId: operationId,
      OperationPreferences: operationPreferences,
      Action: action,
      Version: version
    };
    if (regions && regions.length > 0) {
      processInArray(regions, queryParamsAvailable, 'Regions');
    }
    if (accounts && accounts.length > 0) {
      processInArray(accounts, queryParamsAvailable, 'Accounts');
    }
    if (parameterOverrides && parameterOverrides.length > 0) {
      processInArray(parameterOverrides, queryParamsAvailable, 'ParameterOverrides');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'createStackInstances', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createStackInstancesSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Updates the parameter values for stack instances for the specified accounts, within the specified Regions.
   *
   * @function updateStackInstances
   * @param {string} stackSetName - The name or unique ID of the stack set associated with the stack instances.
   * @param {array} regions - The names of one or more Regions in which you want to update parameter values for stack instances.
   * @param {array} accounts - The names of one or more AWS accounts for which you want to update parameter values for stack instances.
   * @param {object} deploymentTargets - The AWS Organizations accounts for which you want to update parameter values for stack instances.
   * @param {string} operationId - The unique identifier for this stack set operation.
   * @param {object} operationPreferences - Preferences for how AWS CloudFormation performs this stack set operation.
   * @param {array} parameterOverrides - A list of input parameters whose values you want to update for the specified stack instances.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateStackInstances(stackSetName, regions, accounts, deploymentTargets, operationId, operationPreferences, parameterOverrides, callback) {
    this.updateStackInstancesSTSRole(stackSetName, regions, accounts, deploymentTargets, operationId, operationPreferences, parameterOverrides, null, null, callback);
  }

  /**
   * @summary Updates the parameter values for stack instances for the specified accounts, within the specified Regions.
   *
   * @function updateStackInstancesSTSRole
   * @param {string} stackSetName - The name or unique ID of the stack set associated with the stack instances.
   * @param {array} regions - The names of one or more Regions in which you want to update parameter values for stack instances.
   * @param {array} accounts - The names of one or more AWS accounts for which you want to update parameter values for stack instances.
   * @param {object} deploymentTargets - The AWS Organizations accounts for which you want to update parameter values for stack instances.
   * @param {string} operationId - The unique identifier for this stack set operation.
   * @param {object} operationPreferences - Preferences for how AWS CloudFormation performs this stack set operation.
   * @param {array} parameterOverrides - A list of input parameters whose values you want to update for the specified stack instances.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateStackInstancesSTSRole(stackSetName, regions, accounts, deploymentTargets, operationId, operationPreferences, parameterOverrides, stsParams, roleName, callback) {
    const meth = 'adapter-updateStackInstancesSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'UpdateStackInstances';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (regions === undefined || regions === null || !Array.isArray(regions) || regions.length < 1) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['regions'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      DeploymentTargets: deploymentTargets,
      OperationId: operationId,
      OperationPreferences: operationPreferences,
      Action: action,
      Version: version
    };
    if (regions && regions.length > 0) {
      processInArray(regions, queryParamsAvailable, 'Regions');
    }
    if (accounts && accounts.length > 0) {
      processInArray(accounts, queryParamsAvailable, 'Accounts');
    }
    if (parameterOverrides && parameterOverrides.length > 0) {
      processInArray(parameterOverrides, queryParamsAvailable, 'ParameterOverrides');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'updateStackInstances', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateStackInstancesSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Deletes stack instances for the specified accounts, in the specified regions.
   *
   * @function deleteStackInstances
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to delete stack instances for.
   * @param {array} regions - The regions where you want to delete stack set instances.
   * @param {boolean} retainStacks - Removes the stack instances from the specified stack set, but doesn't delete the stacks.
   * @param {array} accounts - The names of the AWS accounts that you want to delete stack instances for.
   * @param {object} deploymentTargets - The AWS Organizations accounts from which to delete stack instances.
   * @param {string} operationId - The unique identifier for this stack set operation.
   * @param {object} operationPreferences - Preferences for how AWS CloudFormation performs this stack set operation.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteStackInstances(stackSetName, regions, retainStacks, accounts, deploymentTargets, operationId, operationPreferences, callback) {
    this.deleteStackInstancesSTSRole(stackSetName, regions, retainStacks, accounts, deploymentTargets, operationId, operationPreferences, null, null, callback);
  }

  /**
   * @summary Deletes stack instances for the specified accounts, in the specified regions.
   *
   * @function deleteStackInstancesSTSRole
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to delete stack instances for.
   * @param {array} regions - The regions where you want to delete stack set instances.
   * @param {boolean} retainStacks - Removes the stack instances from the specified stack set, but doesn't delete the stacks.
   * @param {array} accounts - The names of the AWS accounts that you want to delete stack instances for.
   * @param {object} deploymentTargets - The AWS Organizations accounts from which to delete stack instances.
   * @param {string} operationId - The unique identifier for this stack set operation.
   * @param {object} operationPreferences - Preferences for how AWS CloudFormation performs this stack set operation.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteStackInstancesSTSRole(stackSetName, regions, retainStacks, accounts, deploymentTargets, operationId, operationPreferences, stsParams, roleName, callback) {
    const meth = 'adapter-deleteStackInstancesSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DeleteStackInstances';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (regions === undefined || regions === null || !Array.isArray(regions) || regions.length < 1) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['regions'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (retainStacks === undefined || retainStacks === null || retainStacks === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['retainStacks'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      RetainStacks: retainStacks,
      DeploymentTargets: deploymentTargets,
      OperationId: operationId,
      OperationPreferences: operationPreferences,
      Action: action,
      Version: version
    };
    if (regions && regions.length > 0) {
      processInArray(regions, queryParamsAvailable, 'Regions');
    }
    if (accounts && accounts.length > 0) {
      processInArray(accounts, queryParamsAvailable, 'Accounts');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'deleteStackInstances', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteStackInstancesSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns summary information about stack sets that are associated with the user.
   *
   * @function listStackSets
   * @param {number} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - If the previous paginated request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token.
   * @param {string} status - The status of the stack sets that you want to get summary information about.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listStackSets(maxResults, nextToken, status, callback) {
    this.listStackSetsSTSRole(maxResults, nextToken, status, null, null, callback);
  }

  /**
   * @summary Returns summary information about stack sets that are associated with the user.
   *
   * @function listStackSetsSTSRole
   * @param {number} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - If the previous paginated request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token.
   * @param {string} status - The status of the stack sets that you want to get summary information about.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listStackSetsSTSRole(maxResults, nextToken, status, stsParams, roleName, callback) {
    const meth = 'adapter-listStackSetsSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ListStackSets';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      MaxResults: maxResults,
      NextToken: nextToken,
      Status: status,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'listStackSets', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['listStackSetsSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns the description of the specified stack set.
   *
   * @function describeStackSet
   * @param {string} stackSetName - The name or unique ID of the stack set whose description you want.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackSet(stackSetName, callback) {
    this.describeStackSetSTSRole(stackSetName, null, null, callback);
  }

  /**
   * @summary Returns the description of the specified stack set.
   *
   * @function describeStackSetSTSRole
   * @param {string} stackSetName - The name or unique ID of the stack set whose description you want.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackSetSTSRole(stackSetName, stsParams, roleName, callback) {
    const meth = 'adapter-describeStackSetSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeStackSet';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'describeStackSet', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeStackSetSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Creates a stack set.
   *
   * @function createStackSet
   * @param {string} stackSetName - The name to associate with the stack set.
   * @param {string} administrationRoleARN - The Amazon Resource Number (ARN) of the IAM role to use to create this stack set.
   * @param {object} autoDeployment - Describes whether StackSets automatically deploys to AWS Organizations accounts that are added to the target organization or organizational unit (OU).
   * @param {array} capabilities - In some cases, you must explicitly acknowledge that your stack set template contains certain capabilities in order for AWS CloudFormation to create the stack set and related stack instances.
   * @param {string} clientRequestToken - A unique identifier for this CreateStackSet request.
   * @param {string} description - A description of the stack set.
   * @param {string} executionRoleName - The name of the IAM execution role to use to create the stack set.
   * @param {array} parameters - The input parameters for the stack set template.
   * @param {string} permissionModel - Describes how the IAM roles required for stack set operations are created.
   * @param {array} tags - The key-value pairs to associate with this stack set and the stacks created from it.
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createStackSet(stackSetName, administrationRoleARN, autoDeployment, capabilities, clientRequestToken, description, executionRoleName, parameters, permissionModel, tags, templateBody, templateURL, callback) {
    this.createStackSetSTSRole(stackSetName, administrationRoleARN, autoDeployment, capabilities, clientRequestToken, description, executionRoleName, parameters, permissionModel, tags, templateBody, templateURL, null, null, callback);
  }

  /**
   * @summary Creates a stack set.
   *
   * @function createStackSetSTSRole
   * @param {string} stackSetName - The name to associate with the stack set.
   * @param {string} administrationRoleARN - The Amazon Resource Number (ARN) of the IAM role to use to create this stack set.
   * @param {object} autoDeployment - Describes whether StackSets automatically deploys to AWS Organizations accounts that are added to the target organization or organizational unit (OU).
   * @param {array} capabilities - In some cases, you must explicitly acknowledge that your stack set template contains certain capabilities in order for AWS CloudFormation to create the stack set and related stack instances.
   * @param {string} clientRequestToken - A unique identifier for this CreateStackSet request.
   * @param {string} description - A description of the stack set.
   * @param {string} executionRoleName - The name of the IAM execution role to use to create the stack set.
   * @param {array} parameters - The input parameters for the stack set template.
   * @param {string} permissionModel - Describes how the IAM roles required for stack set operations are created.
   * @param {array} tags - The key-value pairs to associate with this stack set and the stacks created from it.
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createStackSetSTSRole(stackSetName, administrationRoleARN, autoDeployment, capabilities, clientRequestToken, description, executionRoleName, parameters, permissionModel, tags, templateBody, templateURL, stsParams, roleName, callback) {
    const meth = 'adapter-createStackSetSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'CreateStackSet';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      AdministrationRoleARN: administrationRoleARN,
      AutoDeployment: autoDeployment,
      ClientRequestToken: clientRequestToken,
      Description: description,
      ExecutionRoleName: executionRoleName,
      PermissionModel: permissionModel,
      TemplateBody: templateBody,
      TemplateURL: templateURL,
      Action: action,
      Version: version
    };
    if (capabilities && capabilities.length > 0) {
      processInArray(capabilities, queryParamsAvailable, 'Capabilities');
    }
    if (parameters && parameters.length > 0) {
      processInArray(parameters, queryParamsAvailable, 'Parameters');
    }
    if (tags && tags.length > 0) {
      processInArray(tags, queryParamsAvailable, 'Tags');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'createStackSet', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createStackSetSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Updates the stack set, and associated stack instances in the specified accounts and Regions.
   *
   * @function updateStackSet
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to update.
   * @param {array} accounts - The accounts in which to update associated stack instances.
   * @param {string} administrationRoleARN - The Amazon Resource Number (ARN) of the IAM role to use to create this stack set.
   * @param {object} autoDeployment - Describes whether StackSets automatically deploys to AWS Organizations accounts that are added to the target organization or organizational unit (OU).
   * @param {array} capabilities - In some cases, you must explicitly acknowledge that your stack template contains certain capabilities in order for AWS CloudFormation to update the stack set and its associated stack instances.
   * @param {string} deploymentTargets - The AWS Organizations accounts in which to update associated stack instances.
   * @param {string} description - A brief description of updates that you are making.
   * @param {string} executionRoleName - The name of the IAM execution role to use to update the stack set.
   * @param {string} operationId - The unique ID for this stack set operation.
   * @param {object} operationPreferences - Preferences for how AWS CloudFormation performs this stack set operation.
   * @param {array} parameters - A list of input parameters for the stack set template.
   * @param {string} permissionModel - Describes how the IAM roles required for stack set operations are created.
   * @param {array} regions - The Regions in which to update associated stack instances.
   * @param {array} tags - The key-value pairs to associate with this stack set and the stacks created from it.
   * @param {string} templateBody - The structure that contains the template body, with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - The location of the file that contains the template body.
   * @param {boolean} usePreviousTemplate - Use the existing template that's associated with the stack set that you're updating.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateStackSet(stackSetName, accounts, administrationRoleARN, autoDeployment, capabilities, deploymentTargets, description, executionRoleName, operationId, operationPreferences, parameters, permissionModel, regions, tags, templateBody, templateURL, usePreviousTemplate, callback) {
    this.updateStackSetSTSRole(stackSetName, accounts, administrationRoleARN, autoDeployment, capabilities, deploymentTargets, description, executionRoleName, operationId, operationPreferences, parameters, permissionModel, regions, tags, templateBody, templateURL, usePreviousTemplate, null, null, callback);
  }

  /**
   * @summary Updates the stack set, and associated stack instances in the specified accounts and Regions.
   *
   * @function updateStackSetSTSRole
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to update.
   * @param {array} accounts - The accounts in which to update associated stack instances.
   * @param {string} administrationRoleARN - The Amazon Resource Number (ARN) of the IAM role to use to create this stack set.
   * @param {object} autoDeployment - Describes whether StackSets automatically deploys to AWS Organizations accounts that are added to the target organization or organizational unit (OU).
   * @param {array} capabilities - In some cases, you must explicitly acknowledge that your stack template contains certain capabilities in order for AWS CloudFormation to update the stack set and its associated stack instances.
   * @param {string} deploymentTargets - The AWS Organizations accounts in which to update associated stack instances.
   * @param {string} description - A brief description of updates that you are making.
   * @param {string} executionRoleName - The name of the IAM execution role to use to update the stack set.
   * @param {string} operationId - The unique ID for this stack set operation.
   * @param {object} operationPreferences - Preferences for how AWS CloudFormation performs this stack set operation.
   * @param {array} parameters - A list of input parameters for the stack set template.
   * @param {string} permissionModel - Describes how the IAM roles required for stack set operations are created.
   * @param {array} regions - The Regions in which to update associated stack instances.
   * @param {array} tags - The key-value pairs to associate with this stack set and the stacks created from it.
   * @param {string} templateBody - The structure that contains the template body, with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - The location of the file that contains the template body.
   * @param {boolean} usePreviousTemplate - Use the existing template that's associated with the stack set that you're updating.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateStackSetSTSRole(stackSetName, accounts, administrationRoleARN, autoDeployment, capabilities, deploymentTargets, description, executionRoleName, operationId, operationPreferences, parameters, permissionModel, regions, tags, templateBody, templateURL, usePreviousTemplate, stsParams, roleName, callback) {
    const meth = 'adapter-updateStackSetSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'UpdateStackSet';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      AdministrationRoleARN: administrationRoleARN,
      AutoDeployment: autoDeployment,
      DeploymentTargets: deploymentTargets,
      Description: description,
      ExecutionRoleName: executionRoleName,
      OperationId: operationId,
      OperationPreferences: operationPreferences,
      PermissionModel: permissionModel,
      TemplateBody: templateBody,
      TemplateURL: templateURL,
      UsePreviousTemplate: usePreviousTemplate,
      Action: action,
      Version: version
    };
    if (accounts && accounts.length > 0) {
      processInArray(accounts, queryParamsAvailable, 'Accounts');
    }
    if (capabilities && capabilities.length > 0) {
      processInArray(capabilities, queryParamsAvailable, 'Capabilities');
    }
    if (parameters && parameters.length > 0) {
      processInArray(parameters, queryParamsAvailable, 'Parameters');
    }
    if (regions && regions.length > 0) {
      processInArray(regions, queryParamsAvailable, 'Regions');
    }
    if (tags && tags.length > 0) {
      processInArray(tags, queryParamsAvailable, 'Tags');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'updateStackSet', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['updateStackSetSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Deletes a stack set.
   *
   * @function deleteStackSet
   * @param {string} stackSetName - The name or unique ID of the stack set that you're deleting.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteStackSet(stackSetName, callback) {
    this.deleteStackSetSTSRole(stackSetName, null, null, callback);
  }

  /**
   * @summary Deletes a stack set.
   *
   * @function deleteStackSetSTSRole
   * @param {string} stackSetName - The name or unique ID of the stack set that you're deleting.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteStackSetSTSRole(stackSetName, stsParams, roleName, callback) {
    const meth = 'adapter-deleteStackSetSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DeleteStackSet';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'deleteStackSet', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteStackSetSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Cancels an update on the specified stack. If the call completes successfully, the stack rolls
   * back the update and reverts to the previous stack configuration.
   *
   * @function cancelUpdateStack
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {string} clientRequestToken - A unique identifier for this CancelUpdateStack request.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cancelUpdateStack(stackName, clientRequestToken, callback) {
    this.cancelUpdateStackSTSRole(stackName, clientRequestToken, null, null, callback);
  }

  /**
   * @summary Cancels an update on the specified stack. If the call completes successfully, the stack rolls
   * back the update and reverts to the previous stack configuration.
   *
   * @function cancelUpdateStackSTSRole
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {string} clientRequestToken - A unique identifier for this CancelUpdateStack request.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cancelUpdateStackSTSRole(stackName, clientRequestToken, stsParams, roleName, callback) {
    const meth = 'adapter-cancelUpdateStackSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'CancelUpdateStack';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      ClientRequestToken: clientRequestToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'cancelUpdateStack', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['cancelUpdateStackSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns summary information about operations performed on a stack set.
   *
   * @function listStackSetOperations
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to get operation summaries for.
   * @param {number} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - If the previous paginated request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listStackSetOperations(stackSetName, maxResults, nextToken, callback) {
    this.listStackSetOperationsSTSRole(stackSetName, maxResults, nextToken, null, null, callback);
  }

  /**
   * @summary Returns summary information about operations performed on a stack set.
   *
   * @function listStackSetOperationsSTSRole
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to get operation summaries for.
   * @param {number} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - If the previous paginated request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listStackSetOperationsSTSRole(stackSetName, maxResults, nextToken, stsParams, roleName, callback) {
    const meth = 'adapter-listStackSetOperationsSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ListStackSetOperations';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      MaxResults: maxResults,
      NextToken: nextToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'listStackSetOperations', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['listStackSetOperationsSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns summary information about the results of a stack set operation.
   *
   * @function listStackSetOperationResults
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to get operation results for.
   * @param {string} operationId - The ID of the stack set operation.
   * @param {number} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - If the previous request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listStackSetOperationResults(stackSetName, operationId, maxResults, nextToken, callback) {
    this.listStackSetOperationResultsSTSRole(stackSetName, operationId, maxResults, nextToken, null, null, callback);
  }

  /**
   * @summary Returns summary information about the results of a stack set operation.
   *
   * @function listStackSetOperationResultsSTSRole
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to get operation results for.
   * @param {string} operationId - The ID of the stack set operation.
   * @param {number} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - If the previous request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listStackSetOperationResultsSTSRole(stackSetName, operationId, maxResults, nextToken, stsParams, roleName, callback) {
    const meth = 'adapter-listStackSetOperationResultsSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ListStackSetOperationResults';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (operationId === undefined || operationId === null || operationId === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['operationId'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      OperationId: operationId,
      MaxResults: maxResults,
      NextToken: nextToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'listStackSetOperationResults', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['listStackSetOperationResultsSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns the description of the specified stack set operation.
   *
   * @function describeStackSetOperation
   * @param {string} stackSetName - The name or the unique stack ID of the stack set for the stack operation.
   * @param {string} operationId - The unique ID of the stack set operation.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackSetOperation(stackSetName, operationId, callback) {
    this.describeStackSetOperationSTSRole(stackSetName, operationId, null, null, callback);
  }

  /**
   * @summary Returns the description of the specified stack set operation.
   *
   * @function describeStackSetOperationSTSRole
   * @param {string} stackSetName - The name or the unique stack ID of the stack set for the stack operation.
   * @param {string} operationId - The unique ID of the stack set operation.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackSetOperationSTSRole(stackSetName, operationId, stsParams, roleName, callback) {
    const meth = 'adapter-describeStackSetOperationSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeStackSetOperation';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (operationId === undefined || operationId === null || operationId === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['operationId'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      OperationId: operationId,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'describeStackSetOperation', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeStackSetOperationSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Stops an in-progress operation on a stack set and its associated stack instances.
   *
   * @function stopStackSetOperation
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to stop the operation for.
   * @param {string} operationId - The ID of the stack operation.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  stopStackSetOperation(stackSetName, operationId, callback) {
    this.stopStackSetOperationSTSRole(stackSetName, operationId, null, null, callback);
  }

  /**
   * @summary Stops an in-progress operation on a stack set and its associated stack instances.
   *
   * @function stopStackSetOperationSTSRole
   * @param {string} stackSetName - The name or unique ID of the stack set that you want to stop the operation for.
   * @param {string} operationId - The ID of the stack operation.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  stopStackSetOperationSTSRole(stackSetName, operationId, stsParams, roleName, callback) {
    const meth = 'adapter-stopStackSetOperationSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'StopStackSetOperation';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (operationId === undefined || operationId === null || operationId === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['operationId'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      OperationId: operationId,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'stopStackSetOperation', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['stopStackSetOperationSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns information about a stack drift detection operation.
   *
   * @function describeStackEvents
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {string} nextToken - A string that identifies the next page of events that you want to retrieve.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackEvents(stackName, nextToken, callback) {
    this.describeStackEventsSTSRole(stackName, nextToken, null, null, callback);
  }

  /**
   * @summary Returns information about a stack drift detection operation.
   *
   * @function describeStackEventsSTSRole
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {string} nextToken - A string that identifies the next page of events that you want to retrieve.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackEventsSTSRole(stackName, nextToken, stsParams, roleName, callback) {
    const meth = 'adapter-describeStackEventsSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeStackEvents';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      NextToken: nextToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'describeStackEvents', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeStackEventsSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns descriptions of all resources of the specified stack.
   *
   * @function listStackResources
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack, which are not always interchangeable.
   * @param {string} nextToken - A string that identifies the next page of stack resources that you want to retrieve.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listStackResources(stackName, nextToken, callback) {
    this.listStackResourcesSTSRole(stackName, nextToken, null, null, callback);
  }

  /**
   * @summary Returns descriptions of all resources of the specified stack.
   *
   * @function listStackResourcesSTSRole
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack, which are not always interchangeable.
   * @param {string} nextToken - A string that identifies the next page of stack resources that you want to retrieve.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listStackResourcesSTSRole(stackName, nextToken, stsParams, roleName, callback) {
    const meth = 'adapter-listStackResourcesSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ListStackResources';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      NextToken: nextToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'listStackResources', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['listStackResourcesSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns a description of the specified resource in the specified stack.
   *
   * @function describeStackResource
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {string} logicalResourceId - The logical name of the resource as specified in the template.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackResource(stackName, logicalResourceId, callback) {
    this.describeStackResourceSTSRole(stackName, logicalResourceId, null, null, callback);
  }

  /**
   * @summary Returns a description of the specified resource in the specified stack.
   *
   * @function describeStackResourceSTSRole
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {string} logicalResourceId - The logical name of the resource as specified in the template.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackResourceSTSRole(stackName, logicalResourceId, stsParams, roleName, callback) {
    const meth = 'adapter-describeStackResourceSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeStackResource';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (logicalResourceId === undefined || logicalResourceId === null || logicalResourceId === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['logicalResourceId'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      LogicalResourceId: logicalResourceId,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'describeStackResource', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeStackResourceSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns a description of the specified resource in the specified stack.
   *
   * @function describeStackResources
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {string} logicalResourceId - The logical name of the resource as specified in the template.
   * @param {string} physicalResourceId - The name or unique identifier that corresponds to a physical instance ID of a resource supported by AWS CloudFormation.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackResources(stackName, logicalResourceId, physicalResourceId, callback) {
    this.describeStackResourcesSTSRole(stackName, logicalResourceId, physicalResourceId, null, null, callback);
  }

  /**
   * @summary Returns a description of the specified resource in the specified stack.
   *
   * @function describeStackResourcesSTSRole
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {string} logicalResourceId - The logical name of the resource as specified in the template.
   * @param {string} physicalResourceId - The name or unique identifier that corresponds to a physical instance ID of a resource supported by AWS CloudFormation.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackResourcesSTSRole(stackName, logicalResourceId, physicalResourceId, stsParams, roleName, callback) {
    const meth = 'adapter-describeStackResourcesSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeStackResources';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      LogicalResourceId: logicalResourceId,
      PhysicalResourceId: physicalResourceId,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'describeStackResources', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeStackResourcesSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns drift information for the resources that have been checked for drift in the specified stack.
   *
   * @function describeStackResourceDrifts
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {string} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - A string that identifies the next page of stack resource drift results.
   * @param {array} stackResourceDriftStatusFilters - The resource drift status values to use as filters for the resource drift results returned.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackResourceDrifts(stackName, maxResults, nextToken, stackResourceDriftStatusFilters, callback) {
    this.describeStackResourceDriftsSTSRole(stackName, maxResults, nextToken, stackResourceDriftStatusFilters, null, null, callback);
  }

  /**
   * @summary Returns drift information for the resources that have been checked for drift in the specified stack.
   *
   * @function describeStackResourceDriftsSTSRole
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack.
   * @param {string} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - A string that identifies the next page of stack resource drift results.
   * @param {array} stackResourceDriftStatusFilters - The resource drift status values to use as filters for the resource drift results returned.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackResourceDriftsSTSRole(stackName, maxResults, nextToken, stackResourceDriftStatusFilters, stsParams, roleName, callback) {
    const meth = 'adapter-describeStackResourceDriftsSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeStackResourceDrifts';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      MaxResults: maxResults,
      NextToken: nextToken,
      Action: action,
      Version: version
    };
    if (stackResourceDriftStatusFilters && stackResourceDriftStatusFilters.length > 0) {
      processInArray(stackResourceDriftStatusFilters, queryParamsAvailable, 'StackResourceDriftStatusFilters');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'describeStackResourceDrifts', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeStackResourceDriftsSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns information about whether a resource's actual configuration differs, or has drifted, from it's expected configuration, as defined in the stack template and any values specified as template parameters.
   *
   * @function detectStackResourceDrift
   * @param {string} stackName - The name of the stack to which the resource belongs.
   * @param {string} logicalResourceId - The logical name of the resource for which to return drift information.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  detectStackResourceDrift(stackName, logicalResourceId, callback) {
    this.detectStackResourceDriftSTSRole(stackName, logicalResourceId, null, null, callback);
  }

  /**
   * @summary Returns information about whether a resource's actual configuration differs, or has drifted, from it's expected configuration, as defined in the stack template and any values specified as template parameters.
   *
   * @function detectStackResourceDriftSTSRole
   * @param {string} stackName - The name of the stack to which the resource belongs.
   * @param {string} logicalResourceId - The logical name of the resource for which to return drift information.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  detectStackResourceDriftSTSRole(stackName, logicalResourceId, stsParams, roleName, callback) {
    const meth = 'adapter-detectStackResourceDriftSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DetectStackResourceDrift';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (logicalResourceId === undefined || logicalResourceId === null || logicalResourceId === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['logicalResourceId'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      LogicalResourceId: logicalResourceId,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'detectStackResourceDrift', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['detectStackResourceDriftSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns information about a stack drift detection operation.
   *
   * @function describeStackDriftDetectionStatus
   * @param {string} stackDriftDetectionId - The ID of the drift detection results of this operation.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackDriftDetectionStatus(stackDriftDetectionId, callback) {
    this.describeStackDriftDetectionStatusSTSRole(stackDriftDetectionId, null, null, callback);
  }

  /**
   * @summary Returns information about a stack drift detection operation.
   *
   * @function describeStackDriftDetectionStatusSTSRole
   * @param {string} stackDriftDetectionId - The ID of the drift detection results of this operation.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeStackDriftDetectionStatusSTSRole(stackDriftDetectionId, stsParams, roleName, callback) {
    const meth = 'adapter-describeStackDriftDetectionStatusSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeStackDriftDetectionStatus';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackDriftDetectionId === undefined || stackDriftDetectionId === null || stackDriftDetectionId === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackDriftDetectionId'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackDriftDetectionId: stackDriftDetectionId,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'describeStackDriftDetectionStatus', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeStackDriftDetectionStatusSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Detects whether a stack's actual configuration differs, or has drifted, from it's expected configuration, as defined in the stack template and any values specified as template parameters.
   *
   * @function detectStackDrift
   * @param {string} stackName - The name of the stack for which you want to detect drift
   * @param {array} logicalResourceIds - The logical names of any resources you want to use as filters.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  detectStackDrift(stackName, logicalResourceIds, callback) {
    this.detectStackDriftSTSRole(stackName, logicalResourceIds, null, null, callback);
  }

  /**
   * @summary Detects whether a stack's actual configuration differs, or has drifted, from it's expected configuration, as defined in the stack template and any values specified as template parameters.
   *
   * @function detectStackDriftSTSRole
   * @param {string} stackName - The name of the stack for which you want to detect drift
   * @param {array} logicalResourceIds - The logical names of any resources you want to use as filters.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  detectStackDriftSTSRole(stackName, logicalResourceIds, stsParams, roleName, callback) {
    const meth = 'adapter-detectStackDriftSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DetectStackDrift';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      Action: action,
      Version: version
    };
    if (logicalResourceIds && logicalResourceIds.length > 0) {
      processInArray(logicalResourceIds, queryParamsAvailable, 'LogicalResourceIds');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'detectStackDrift', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['detectStackDriftSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Detect drift on a stack set.
   *
   * @function detectStackSetDrift
   * @param {string} stackSetName - The name of the stack set on which to perform the drift detection operation.
   * @param {string} operationId - The ID of the stack set operation.
   * @param {object} operationPreferences - The user-specified preferences for how AWS CloudFormation performs a stack set operation.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  detectStackSetDrift(stackSetName, operationId, operationPreferences, callback) {
    this.detectStackSetDriftSTSRole(stackSetName, operationId, operationPreferences, null, null, callback);
  }

  /**
   * @summary Detect drift on a stack set.
   *
   * @function detectStackSetDriftSTSRole
   * @param {string} stackSetName - The name of the stack set on which to perform the drift detection operation.
   * @param {string} operationId - The ID of the stack set operation.
   * @param {object} operationPreferences - The user-specified preferences for how AWS CloudFormation performs a stack set operation.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  detectStackSetDriftSTSRole(stackSetName, operationId, operationPreferences, stsParams, roleName, callback) {
    const meth = 'adapter-detectStackSetDriftSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DetectStackSetDrift';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackSetName: stackSetName,
      OperationId: operationId,
      OperationPreferences: operationPreferences,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'detectStackSetDrift', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['detectStackSetDriftSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns the stack policy for a specified stack.
   *
   * @function getStackPolicy
   * @param {string} stackName - The name or unique stack ID that is associated with the stack whose policy you want to get.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getStackPolicy(stackName, callback) {
    this.getStackPolicySTSRole(stackName, null, null, callback);
  }

  /**
   * @summary Returns the stack policy for a specified stack.
   *
   * @function getStackPolicySTSRole
   * @param {string} stackName - The name or unique stack ID that is associated with the stack whose policy you want to get.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getStackPolicySTSRole(stackName, stsParams, roleName, callback) {
    const meth = 'adapter-getStackPolicySTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'GetStackPolicy';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'getStackPolicy', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getStackPolicySTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Sets a stack policy for a specified stack.
   *
   * @function setStackPolicy
   * @param {string} stackName - The name or unique stack ID that you want to associate a policy with.
   * @param {string} stackPolicyBody - Structure containing the stack policy body.
   * @param {string} stackPolicyURL - Location of a file containing the stack policy.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setStackPolicy(stackName, stackPolicyBody, stackPolicyURL, callback) {
    this.setStackPolicySTSRole(stackName, stackPolicyBody, stackPolicyURL, null, null, callback);
  }

  /**
   * @summary Sets a stack policy for a specified stack.
   *
   * @function setStackPolicySTSRole
   * @param {string} stackName - The name or unique stack ID that you want to associate a policy with.
   * @param {string} stackPolicyBody - Structure containing the stack policy body.
   * @param {string} stackPolicyURL - Location of a file containing the stack policy.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setStackPolicySTSRole(stackName, stackPolicyBody, stackPolicyURL, stsParams, roleName, callback) {
    const meth = 'adapter-setStackPolicySTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'SetStackPolicy';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      StackPolicyBody: stackPolicyBody,
      StackPolicyURL: stackPolicyURL,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'setStackPolicy', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['setStackPolicySTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary For a specified stack that is in the UPDATE_ROLLBACK_FAILED state, continues rolling it back to the UPDATE_ROLLBACK_COMPLETE state.
   *
   * @function continueUpdateRollback
   * @param {string} stackName - The name or the unique ID of the stack that you want to continue rolling back.
   * @param {string} clientRequestToken - A unique identifier for this ContinueUpdateRollback request.
   * @param {string} roleARN - The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that AWS CloudFormation assumes to roll back the stack.
   * @param {array} resourcesToSkip - A list of the logical IDs of the resources that AWS CloudFormation skips during the continue update rollback operation.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  continueUpdateRollback(stackName, clientRequestToken, roleARN, resourcesToSkip, callback) {
    this.continueUpdateRollbackSTSRole(stackName, clientRequestToken, roleARN, resourcesToSkip, null, null, callback);
  }

  /**
   * @summary For a specified stack that is in the UPDATE_ROLLBACK_FAILED state, continues rolling it back to the UPDATE_ROLLBACK_COMPLETE state.
   *
   * @function continueUpdateRollbackSTSRole
   * @param {string} stackName - The name or the unique ID of the stack that you want to continue rolling back.
   * @param {string} clientRequestToken - A unique identifier for this ContinueUpdateRollback request.
   * @param {string} roleARN - The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that AWS CloudFormation assumes to roll back the stack.
   * @param {array} resourcesToSkip - A list of the logical IDs of the resources that AWS CloudFormation skips during the continue update rollback operation.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  continueUpdateRollbackSTSRole(stackName, clientRequestToken, roleARN, resourcesToSkip, stsParams, roleName, callback) {
    const meth = 'adapter-continueUpdateRollbackSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ContinueUpdateRollback';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      ClientRequestToken: clientRequestToken,
      RoleARN: roleARN,
      Action: action,
      Version: version
    };
    if (resourcesToSkip && resourcesToSkip.length > 0) {
      processInArray(resourcesToSkip, queryParamsAvailable, 'ResourcesToSkip');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Rollback', 'continueUpdateRollback', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['continueUpdateRollbackSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns the ID and status of each active change set for a stack.
   *
   * @function listChangeSets
   * @param {string} stackName - The name or the Amazon Resource Name (ARN) of the stack for which you want to list change sets.
   * @param {string} nextToken - A string (provided by the ListChangeSets response output) that identifies the next page of change sets that you want to retrieve.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listChangeSets(stackName, nextToken, callback) {
    this.listChangeSetsSTSRole(stackName, nextToken, null, null, callback);
  }

  /**
   * @summary Returns the ID and status of each active change set for a stack.
   *
   * @function listChangeSetsSTSRole
   * @param {string} stackName - The name or the Amazon Resource Name (ARN) of the stack for which you want to list change sets.
   * @param {string} nextToken - A string (provided by the ListChangeSets response output) that identifies the next page of change sets that you want to retrieve.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listChangeSetsSTSRole(stackName, nextToken, stsParams, roleName, callback) {
    const meth = 'adapter-listChangeSetsSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ListChangeSets';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      NextToken: nextToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('ChangeSet', 'listChangeSets', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['listChangeSetsSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns the inputs for the change set and a list of changes that AWS CloudFormation will make if you execute the change set.
   *
   * @function describeChangeSet
   * @param {string} changeSetName - The name or Amazon Resource Name (ARN) of the change set that you want to describe.
   * @param {string} stackName - If you specified the name of a change set, specify the stack name or ID (ARN) of the change set you want to describe.
   * @param {string} nextToken - A string (provided by the DescribeChangeSet response output) that identifies the next page of information that you want to retrieve.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeChangeSet(changeSetName, stackName, nextToken, callback) {
    this.describeChangeSetSTSRole(changeSetName, stackName, nextToken, null, null, callback);
  }

  /**
   * @summary Returns the inputs for the change set and a list of changes that AWS CloudFormation will make if you execute the change set.
   *
   * @function describeChangeSetSTSRole
   * @param {string} changeSetName - The name or Amazon Resource Name (ARN) of the change set that you want to describe.
   * @param {string} stackName - If you specified the name of a change set, specify the stack name or ID (ARN) of the change set you want to describe.
   * @param {string} nextToken - A string (provided by the DescribeChangeSet response output) that identifies the next page of information that you want to retrieve.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeChangeSetSTSRole(changeSetName, stackName, nextToken, stsParams, roleName, callback) {
    const meth = 'adapter-describeChangeSetSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeChangeSet';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (changeSetName === undefined || changeSetName === null || changeSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['changeSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      ChangeSetName: changeSetName,
      StackName: stackName,
      NextToken: nextToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('ChangeSet', 'describeChangeSet', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeChangeSetSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Updates a stack using the input information that was provided when the specified change set was created.
   *
   * @function executeChangeSet
   * @param {string} changeSetName - The name or ARN of the change set that you want use to update the specified stack.
   * @param {string} stackName - If you specified the name of a change set, specify the stack name or ID (ARN) that is associated with the change set you want to execute.
   * @param {string} clientRequestToken - A unique identifier for this ExecuteChangeSet request.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  executeChangeSet(changeSetName, stackName, clientRequestToken, callback) {
    this.executeChangeSetSTSRole(changeSetName, stackName, clientRequestToken, null, null, callback);
  }

  /**
   * @summary Updates a stack using the input information that was provided when the specified change set was created.
   *
   * @function executeChangeSetSTSRole
   * @param {string} changeSetName - The name or ARN of the change set that you want use to update the specified stack.
   * @param {string} stackName - If you specified the name of a change set, specify the stack name or ID (ARN) that is associated with the change set you want to execute.
   * @param {string} clientRequestToken - A unique identifier for this ExecuteChangeSet request.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  executeChangeSetSTSRole(changeSetName, stackName, clientRequestToken, stsParams, roleName, callback) {
    const meth = 'adapter-executeChangeSetSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ExecuteChangeSet';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (changeSetName === undefined || changeSetName === null || changeSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['changeSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      ChangeSetName: changeSetName,
      StackName: stackName,
      ClientRequestToken: clientRequestToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('ChangeSet', 'executeChangeSet', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['executeChangeSetSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Creates a list of changes that will be applied to a stack so that you can review the changes before executing them.
   *
   * @function createChangeSet
   * @param {string} stackName - The name or the unique ID of the stack for which you are creating a change set.
   * @param {string} changeSetName - The name of the change set.
   * @param {array} capabilities - In some cases, you must explicitly acknowledge that your stack template contains certain capabilities in order for AWS CloudFormation to create the stack.
   * @param {string} changeSetType - The type of change set operation.
   * @param {string} clientToken - A unique identifier for this CreateChangeSet request.
   * @param {string} description - A description to help you identify this change set.
   * @param {array} notificationARNs - The Amazon Resource Names (ARNs) of Amazon Simple Notification Service (Amazon SNS) topics that AWS CloudFormation associates with the stack.
   * @param {array} parameters - A list of Parameter structures that specify input parameters for the change set. For more information, see the Parameter data type.
   * @param {array} resourcesToImport - A list of Parameter structures that specify input parameters for the change set. For more information, see the Parameter data type.
   * @param {array} resourceTypes - The template resource types that you have permissions to work with for this create stack action.
   * @param {string} roleARN - The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that AWS CloudFormation assumes to roll back the stack.
   * @param {object} rollbackConfiguration - The rollback triggers for AWS CloudFormation to monitor during stack creation and updating operations, and for the specified monitoring period afterwards.
   * @param {array} tags - Key-value pairs to associate with this stack.
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {boolean} usePreviousTemplate - Whether to reuse the template that is associated with the stack to create the change set.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createChangeSet(stackName, changeSetName, capabilities, changeSetType, clientToken, description, notificationARNs, parameters, resourcesToImport, resourceTypes, roleARN, rollbackConfiguration, tags, templateBody, templateURL, usePreviousTemplate, callback) {
    this.createChangeSetSTSRole(stackName, changeSetName, capabilities, changeSetType, clientToken, description, notificationARNs, parameters, resourcesToImport, resourceTypes, roleARN, rollbackConfiguration, tags, templateBody, templateURL, usePreviousTemplate, null, null, callback);
  }

  /**
   * @summary Creates a list of changes that will be applied to a stack so that you can review the changes before executing them.
   *
   * @function createChangeSetSTSRole
   * @param {string} stackName - The name or the unique ID of the stack for which you are creating a change set.
   * @param {string} changeSetName - The name of the change set.
   * @param {array} capabilities - In some cases, you must explicitly acknowledge that your stack template contains certain capabilities in order for AWS CloudFormation to create the stack.
   * @param {string} changeSetType - The type of change set operation.
   * @param {string} clientToken - A unique identifier for this CreateChangeSet request.
   * @param {string} description - A description to help you identify this change set.
   * @param {array} notificationARNs - The Amazon Resource Names (ARNs) of Amazon Simple Notification Service (Amazon SNS) topics that AWS CloudFormation associates with the stack.
   * @param {array} parameters - A list of Parameter structures that specify input parameters for the change set. For more information, see the Parameter data type.
   * @param {array} resourcesToImport - A list of Parameter structures that specify input parameters for the change set. For more information, see the Parameter data type.
   * @param {array} resourceTypes - The template resource types that you have permissions to work with for this create stack action.
   * @param {string} roleARN - The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that AWS CloudFormation assumes to roll back the stack.
   * @param {object} rollbackConfiguration - The rollback triggers for AWS CloudFormation to monitor during stack creation and updating operations, and for the specified monitoring period afterwards.
   * @param {array} tags - Key-value pairs to associate with this stack.
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {boolean} usePreviousTemplate - Whether to reuse the template that is associated with the stack to create the change set.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createChangeSetSTSRole(stackName, changeSetName, capabilities, changeSetType, clientToken, description, notificationARNs, parameters, resourcesToImport, resourceTypes, roleARN, rollbackConfiguration, tags, templateBody, templateURL, usePreviousTemplate, stsParams, roleName, callback) {
    const meth = 'adapter-createChangeSetSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'CreateChangeSet';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (changeSetName === undefined || changeSetName === null || changeSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['changeSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      ChangeSetName: changeSetName,
      ChangeSetType: changeSetType,
      ClientToken: clientToken,
      Description: description,
      RoleARN: roleARN,
      RollbackConfiguration: rollbackConfiguration,
      TemplateBody: templateBody,
      TemplateURL: templateURL,
      UsePreviousTemplate: usePreviousTemplate,
      Action: action,
      Version: version
    };
    if (capabilities && capabilities.length > 0) {
      processInArray(capabilities, queryParamsAvailable, 'Capabilities');
    }
    if (notificationARNs && notificationARNs.length > 0) {
      processInArray(notificationARNs, queryParamsAvailable, 'NotificationARNs');
    }
    if (parameters && parameters.length > 0) {
      processInArray(parameters, queryParamsAvailable, 'Parameters');
    }
    if (resourcesToImport && resourcesToImport.length > 0) {
      processInArray(resourcesToImport, queryParamsAvailable, 'ResourcesToImport');
    }
    if (resourceTypes && resourceTypes.length > 0) {
      processInArray(resourceTypes, queryParamsAvailable, 'ResourceTypes');
    }
    if (tags && tags.length > 0) {
      processInArray(tags, queryParamsAvailable, 'Tags');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('ChangeSet', 'createChangeSet', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['createChangeSetSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Deletes the specified change set. Deleting change sets ensures that no one executes the wrong change set.
   *
   * @function deleteChangeSet
   * @param {string} changeSetName - The name or Amazon Resource Name (ARN) of the change set that you want to delete.
   * @param {string} stackName - If you specified the name of a change set to delete, specify the stack name or ID (ARN) that is associated with it.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteChangeSet(changeSetName, stackName, callback) {
    this.deleteChangeSetSTSRole(changeSetName, stackName, null, null, callback);
  }

  /**
   * @summary Deletes the specified change set. Deleting change sets ensures that no one executes the wrong change set.
   *
   * @function deleteChangeSetSTSRole
   * @param {string} changeSetName - The name or Amazon Resource Name (ARN) of the change set that you want to delete.
   * @param {string} stackName - If you specified the name of a change set to delete, specify the stack name or ID (ARN) that is associated with it.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteChangeSetSTSRole(changeSetName, stackName, stsParams, roleName, callback) {
    const meth = 'adapter-deleteChangeSetSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DeleteChangeSet';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (changeSetName === undefined || changeSetName === null || changeSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['changeSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      ChangeSetName: changeSetName,
      StackName: stackName,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('ChangeSet', 'deleteChangeSet', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deleteChangeSetSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns summary information about types that have been registered with CloudFormation.
   *
   * @function listTypes
   * @param {string} deprecatedStatus - The deprecation status of the types that you want to get summary information about.
   * @param {number} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - If the previous paginated request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token.
   * @param {string} provisioningType - The provisioning behavior of the type.
   * @param {string} visibility - The scope at which the type is visible and usable in CloudFormation operations.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listTypes(deprecatedStatus, maxResults, nextToken, provisioningType, visibility, callback) {
    this.listTypesSTSRole(deprecatedStatus, maxResults, nextToken, provisioningType, visibility, null, null, callback);
  }

  /**
   * @summary Returns summary information about types that have been registered with CloudFormation.
   *
   * @function listTypesSTSRole
   * @param {string} deprecatedStatus - The deprecation status of the types that you want to get summary information about.
   * @param {number} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - If the previous paginated request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token.
   * @param {string} provisioningType - The provisioning behavior of the type.
   * @param {string} visibility - The scope at which the type is visible and usable in CloudFormation operations.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listTypesSTSRole(deprecatedStatus, maxResults, nextToken, provisioningType, visibility, stsParams, roleName, callback) {
    const meth = 'adapter-listTypesSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ListTypes';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      DeprecatedStatus: deprecatedStatus,
      MaxResults: maxResults,
      NextToken: nextToken,
      ProvisioningType: provisioningType,
      Visibility: visibility,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'listTypes', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['listTypesSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns detailed information about a type that has been registered.
   *
   * @function describeType
   * @param {string} arn - The Amazon Resource Name (ARN) of the type.
   * @param {string} type - The kind of type.
   * @param {string} typeName - The name of the type.
   * @param {string} versionId - The ID of a specific version of the type.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeType(arn, type, typeName, versionId, callback) {
    this.describeTypeSTSRole(arn, type, typeName, versionId, null, null, callback);
  }

  /**
   * @summary Returns detailed information about a type that has been registered.
   *
   * @function describeTypeSTSRole
   * @param {string} arn - The Amazon Resource Name (ARN) of the type.
   * @param {string} type - The kind of type.
   * @param {string} typeName - The name of the type.
   * @param {string} versionId - The ID of a specific version of the type.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeTypeSTSRole(arn, type, typeName, versionId, stsParams, roleName, callback) {
    const meth = 'adapter-describeTypeSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeType';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      Arn: arn,
      Type: type,
      TypeName: typeName,
      VersionId: versionId,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'describeType', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeTypeSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns a list of registration tokens for the specified type(s).
   *
   * @function listTypeRegistrations
   * @param {number} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - TIf the previous paginated request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token.
   * @param {string} registrationStatusFilter - The current status of the type registration request.
   * @param {string} type - The kind of type.
   * @param {string} typeArn - The Amazon Resource Name (ARN) of the type.
   * @param {string} typeName - The name of the type.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listTypeRegistrations(maxResults, nextToken, registrationStatusFilter, type, typeArn, typeName, callback) {
    this.listTypeRegistrationsSTSRole(maxResults, nextToken, registrationStatusFilter, type, typeArn, typeName, null, null, callback);
  }

  /**
   * @summary Returns a list of registration tokens for the specified type(s).
   *
   * @function listTypeRegistrationsSTSRole
   * @param {number} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - TIf the previous paginated request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token.
   * @param {string} registrationStatusFilter - The current status of the type registration request.
   * @param {string} type - The kind of type.
   * @param {string} typeArn - The Amazon Resource Name (ARN) of the type.
   * @param {string} typeName - The name of the type.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listTypeRegistrationsSTSRole(maxResults, nextToken, registrationStatusFilter, type, typeArn, typeName, stsParams, roleName, callback) {
    const meth = 'adapter-listTypeRegistrationsSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ListTypeRegistrations';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      MaxResults: maxResults,
      NextToken: nextToken,
      RegistrationStatusFilter: registrationStatusFilter,
      Type: type,
      TypeArn: typeArn,
      TypeName: typeName,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'listTypeRegistrations', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['listTypeRegistrationsSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns information about a type's registration, including its current status and type and version identifiers.
   *
   * @function describeTypeRegistration
   * @param {string} registrationToken - The identifier for this registration request.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeTypeRegistration(registrationToken, callback) {
    this.describeTypeRegistrationSTSRole(registrationToken, null, null, callback);
  }

  /**
   * @summary Returns information about a type's registration, including its current status and type and version identifiers.
   *
   * @function describeTypeRegistrationSTSRole
   * @param {string} registrationToken - The identifier for this registration request.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeTypeRegistrationSTSRole(registrationToken, stsParams, roleName, callback) {
    const meth = 'adapter-describeTypeRegistrationSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeTypeRegistration';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (registrationToken === undefined || registrationToken === null || registrationToken === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['registrationToken'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      RegistrationToken: registrationToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'describeTypeRegistration', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeTypeRegistrationSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Registers a type with the CloudFormation service.
   *
   * @function registerType
   * @param {string} schemaHandlerPackage - A url to the S3 bucket containing the schema handler package that contains the schema, event handlers, and associated files for the type you want to register.
   * @param {string} clientRequestToken - A unique identifier that acts as an idempotency key for this registration request.
   * @param {string} executionRoleArn - The Amazon Resource Name (ARN) of the IAM execution role to use to register the type.
   * @param {object} loggingConfig - Specifies logging configuration information for a type.
   * @param {string} type - The kind of type.
   * @param {string} typeName - The name of the type being registered.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  registerType(schemaHandlerPackage, clientRequestToken, executionRoleArn, loggingConfig, type, typeName, callback) {
    this.registerTypeSTSRole(schemaHandlerPackage, clientRequestToken, executionRoleArn, loggingConfig, type, typeName, null, null, callback);
  }

  /**
   * @summary Registers a type with the CloudFormation service.
   *
   * @function registerTypeSTSRole
   * @param {string} schemaHandlerPackage - A url to the S3 bucket containing the schema handler package that contains the schema, event handlers, and associated files for the type you want to register.
   * @param {string} clientRequestToken - A unique identifier that acts as an idempotency key for this registration request.
   * @param {string} executionRoleArn - The Amazon Resource Name (ARN) of the IAM execution role to use to register the type.
   * @param {object} loggingConfig - Specifies logging configuration information for a type.
   * @param {string} type - The kind of type.
   * @param {string} typeName - The name of the type being registered.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  registerTypeSTSRole(schemaHandlerPackage, clientRequestToken, executionRoleArn, loggingConfig, type, typeName, stsParams, roleName, callback) {
    const meth = 'adapter-registerTypeSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'RegisterType';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (schemaHandlerPackage === undefined || schemaHandlerPackage === null || schemaHandlerPackage === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['schemaHandlerPackage'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      SchemaHandlerPackage: schemaHandlerPackage,
      ClientRequestToken: clientRequestToken,
      ExecutionRoleArn: executionRoleArn,
      LoggingConfig: loggingConfig,
      Type: type,
      TypeName: typeName,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'registerType', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['registerTypeSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Removes a type or type version from active use in the CloudFormation registry.
   *
   * @function deregisterType
   * @param {string} arn - The Amazon Resource Name (ARN) of the type.
   * @param {string} type - The kind of type.
   * @param {string} typeName - The name of the type.
   * @param {string} versionId - The ID of a specific version of the type.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deregisterType(arn, type, typeName, versionId, callback) {
    this.deregisterTypeSTSRole(arn, type, typeName, versionId, null, null, callback);
  }

  /**
   * @summary Removes a type or type version from active use in the CloudFormation registry.
   *
   * @function deregisterTypeSTSRole
   * @param {string} arn - The Amazon Resource Name (ARN) of the type.
   * @param {string} type - The kind of type.
   * @param {string} typeName - The name of the type.
   * @param {string} versionId - The ID of a specific version of the type.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deregisterTypeSTSRole(arn, type, typeName, versionId, stsParams, roleName, callback) {
    const meth = 'adapter-deregisterTypeSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DeregisterType';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      Arn: arn,
      Type: type,
      TypeName: typeName,
      VersionId: versionId,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'deregisterType', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deregisterTypeSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns summary information about the versions of a type.
   *
   * @function listTypeVersions
   * @param {string} arn - The Amazon Resource Name (ARN) of the type for which you want version summary information.
   * @param {string} deprecatedStatus - The deprecation status of the type versions that you want to get summary information about.
   * @param {number} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - If the previous paginated request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token.
   * @param {string} type - The kind of type.
   * @param {string} typeName - The name of the type for which you want version summary information.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listTypeVersions(arn, deprecatedStatus, maxResults, nextToken, type, typeName, callback) {
    this.listTypeVersionsSTSRole(arn, deprecatedStatus, maxResults, nextToken, type, typeName, null, null, callback);
  }

  /**
   * @summary Returns summary information about the versions of a type.
   *
   * @function listTypeVersionsSTSRole
   * @param {string} arn - The Amazon Resource Name (ARN) of the type for which you want version summary information.
   * @param {string} deprecatedStatus - The deprecation status of the type versions that you want to get summary information about.
   * @param {number} maxResults - The maximum number of results to be returned with a single call.
   * @param {string} nextToken - If the previous paginated request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token.
   * @param {string} type - The kind of type.
   * @param {string} typeName - The name of the type for which you want version summary information.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listTypeVersionsSTSRole(arn, deprecatedStatus, maxResults, nextToken, type, typeName, stsParams, roleName, callback) {
    const meth = 'adapter-listTypeVersionsSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ListTypeVersions';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      Arn: arn,
      DeprecatedStatus: deprecatedStatus,
      MaxResults: maxResults,
      NextToken: nextToken,
      Type: type,
      TypeName: typeName,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'listTypeVersions', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['listTypeVersionsSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Specify the default version of a type.
   *
   * @function setTypeDefaultVersion
   * @param {string} arn - The Amazon Resource Name (ARN) of the type for which you want version summary information.
   * @param {string} type - The kind of type.
   * @param {string} typeName - The name of the type.
   * @param {string} versionId - The ID of a specific version of the type.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setTypeDefaultVersion(arn, type, typeName, versionId, callback) {
    this.setTypeDefaultVersionSTSRole(arn, type, typeName, versionId, null, null, callback);
  }

  /**
   * @summary Specify the default version of a type.
   *
   * @function setTypeDefaultVersionSTSRole
   * @param {string} arn - The Amazon Resource Name (ARN) of the type for which you want version summary information.
   * @param {string} type - The kind of type.
   * @param {string} typeName - The name of the type.
   * @param {string} versionId - The ID of a specific version of the type.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setTypeDefaultVersionSTSRole(arn, type, typeName, versionId, stsParams, roleName, callback) {
    const meth = 'adapter-setTypeDefaultVersionSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'SetTypeDefaultVersion';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      Arn: arn,
      Type: type,
      TypeName: typeName,
      VersionId: versionId,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'setTypeDefaultVersion', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['setTypeDefaultVersionSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Lists all stacks that are importing an exported output value.
   *
   * @function listImports
   * @param {string} exportName - The name of the exported output value.
   * @param {string} nextToken - A string (provided by the ListImports response output) that identifies the next page of stacks that are importing the specified exported output value.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listImports(exportName, nextToken, callback) {
    this.listImportsSTSRole(exportName, nextToken, null, null, callback);
  }

  /**
   * @summary Lists all stacks that are importing an exported output value.
   *
   * @function listImportsSTSRole
   * @param {string} exportName - The name of the exported output value.
   * @param {string} nextToken - A string (provided by the ListImports response output) that identifies the next page of stacks that are importing the specified exported output value.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listImportsSTSRole(exportName, nextToken, stsParams, roleName, callback) {
    const meth = 'adapter-listImportsSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ListImports';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (exportName === undefined || exportName === null || exportName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['exportName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      ExportName: exportName,
      NextToken: nextToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Account', 'listImports', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['listImportsSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Lists all exported output values in the account and region in which you call this action.
   *
   * @function listExports
   * @param {string} nextToken - A string (provided by the ListExports response output) that identifies the next page of exported output values that you asked to retrieve.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listExports(nextToken, callback) {
    this.listExportsSTSRole(nextToken, null, null, callback);
  }

  /**
   * @summary Lists all exported output values in the account and region in which you call this action.
   *
   * @function listExportsSTSRole
   * @param {string} nextToken - A string (provided by the ListExports response output) that identifies the next page of exported output values that you asked to retrieve.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  listExportsSTSRole(nextToken, stsParams, roleName, callback) {
    const meth = 'adapter-listExportsSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ListExports';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      NextToken: nextToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Account', 'listExports', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['listExportsSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Retrieves your account's AWS CloudFormation limits, such as the maximum number of stacks that you can create in your account.
   *
   * @function describeAccountLimits
   * @param {string} nextToken - A string that identifies the next page of limits that you want to retrieve.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeAccountLimits(nextToken, callback) {
    this.describeAccountLimitsSTSRole(nextToken, null, null, callback);
  }

  /**
   * @summary Retrieves your account's AWS CloudFormation limits, such as the maximum number of stacks that you can create in your account.
   *
   * @function describeAccountLimitsSTSRole
   * @param {string} nextToken - A string that identifies the next page of limits that you want to retrieve.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeAccountLimitsSTSRole(nextToken, stsParams, roleName, callback) {
    const meth = 'adapter-describeAccountLimitsSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeAccountLimits';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      NextToken: nextToken,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Account', 'describeAccountLimits', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeAccountLimitsSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns the template body for a specified stack.
   *
   * @function getTemplate
   * @param {string} changeSetName - The name or Amazon Resource Name (ARN) of a change set for which AWS CloudFormation returns the associated template.
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack, which are not always interchangeable:
   * @param {string} templateStage - For templates that include transforms, the stage of the template that AWS CloudFormation returns.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getTemplate(changeSetName, stackName, templateStage, callback) {
    this.getTemplateSTSRole(changeSetName, stackName, templateStage, null, null, callback);
  }

  /**
   * @summary Returns the template body for a specified stack.
   *
   * @function getTemplateSTSRole
   * @param {string} changeSetName - The name or Amazon Resource Name (ARN) of a change set for which AWS CloudFormation returns the associated template.
   * @param {string} stackName - The name or the unique stack ID that is associated with the stack, which are not always interchangeable:
   * @param {string} templateStage - For templates that include transforms, the stage of the template that AWS CloudFormation returns.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getTemplateSTSRole(changeSetName, stackName, templateStage, stsParams, roleName, callback) {
    const meth = 'adapter-getTemplateSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'GetTemplate';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      ChangeSetName: changeSetName,
      StackName: stackName,
      TemplateStage: templateStage,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Template', 'getTemplate', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getTemplateSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns information about a new or existing template.
   *
   * @function getTemplateSummary
   * @param {string} stackName - The name or the stack ID that is associated with the stack, which are not always interchangeable.
   * @param {string} stackSetName - The name or unique ID of the stack set from which the stack was created.
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getTemplateSummary(stackName, stackSetName, templateBody, templateURL, callback) {
    this.getTemplateSummarySTSRole(stackName, stackSetName, templateBody, templateURL, null, null, callback);
  }

  /**
   * @summary Returns information about a new or existing template.
   *
   * @function getTemplateSummarySTSRole
   * @param {string} stackName - The name or the stack ID that is associated with the stack, which are not always interchangeable.
   * @param {string} stackSetName - The name or unique ID of the stack set from which the stack was created.
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getTemplateSummarySTSRole(stackName, stackSetName, templateBody, templateURL, stsParams, roleName, callback) {
    const meth = 'adapter-getTemplateSummarySTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'GetTemplateSummary';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      StackName: stackName,
      StackSetName: stackSetName,
      TemplateBody: templateBody,
      TemplateURL: templateURL,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Template', 'getTemplateSummary', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['getTemplateSummarySTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Validates a specified template.
   *
   * @function validateTemplate
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  validateTemplate(templateBody, templateURL, callback) {
    this.validateTemplateSTSRole(templateBody, templateURL, null, null, callback);
  }

  /**
   * @summary Validates a specified template.
   *
   * @function validateTemplateSTSRole
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  validateTemplateSTSRole(templateBody, templateURL, stsParams, roleName, callback) {
    const meth = 'adapter-validateTemplateSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ValidateTemplate';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      TemplateBody: templateBody,
      TemplateURL: templateURL,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Template', 'validateTemplate', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['validateTemplateSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns the estimated monthly cost of a template.
   *
   * @function estimateTemplateCost
   * @param {array} parameters - A list of Parameter structures that specify input parameters.
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  estimateTemplateCost(parameters, templateBody, templateURL, callback) {
    this.estimateTemplateCostSTSRole(parameters, templateBody, templateURL, null, null, callback);
  }

  /**
   * @summary Returns the estimated monthly cost of a template.
   *
   * @function estimateTemplateCostSTSRole
   * @param {array} parameters - A list of Parameter structures that specify input parameters.
   * @param {string} templateBody - Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes.
   * @param {string} templateURL - Location of file containing the template body.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  estimateTemplateCostSTSRole(parameters, templateBody, templateURL, stsParams, roleName, callback) {
    const meth = 'adapter-estimateTemplateCostSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'EstimateTemplateCost';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      TemplateBody: templateBody,
      TemplateURL: templateURL,
      Action: action,
      Version: version
    };
    if (parameters && parameters.length > 0) {
      processInArray(parameters, queryParamsAvailable, 'Parameters');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Template', 'estimateTemplateCost', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['estimateTemplateCostSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Sends a signal to the specified resource with a success or failure status.
   *
   * @function signalResource
   * @param {array} logicalResourceId - The logical ID of the resource that you want to signal.
   * @param {string} stackName - The stack name or unique stack ID that includes the resource that you want to signal.
   * @param {string} status - The status of the signal, which is either success or failure.
   * @param {string} uniqueId - A unique ID of the signal. When you signal Amazon EC2 instances or Auto Scaling groups, specify the instance ID that you are signaling as the unique ID.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  signalResource(logicalResourceId, stackName, status, uniqueId, callback) {
    this.signalResourceSTSRole(logicalResourceId, stackName, status, uniqueId, null, null, callback);
  }

  /**
   * @summary Sends a signal to the specified resource with a success or failure status.
   *
   * @function signalResourceSTSRole
   * @param {array} logicalResourceId - The logical ID of the resource that you want to signal.
   * @param {string} stackName - The stack name or unique stack ID that includes the resource that you want to signal.
   * @param {string} status - The status of the signal, which is either success or failure.
   * @param {string} uniqueId - A unique ID of the signal. When you signal Amazon EC2 instances or Auto Scaling groups, specify the instance ID that you are signaling as the unique ID.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  signalResourceSTSRole(logicalResourceId, stackName, status, uniqueId, stsParams, roleName, callback) {
    const meth = 'adapter-signalResourceSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'SignalResource';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (logicalResourceId === undefined || logicalResourceId === null || logicalResourceId === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['logicalResourceId'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (status === undefined || status === null || status === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['status'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (uniqueId === undefined || uniqueId === null || uniqueId === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['uniqueId'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      LogicalResourceId: logicalResourceId,
      StackName: stackName,
      Status: status,
      UniqueId: uniqueId,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Resource', 'signalResource', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['signalResourceSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns hook-related information for the change set and a list of changes that AWS CloudFormation makes when you run the change set.
   *
   * @function describeChangeSetHooks
   * @param {string} changeSetName - The name or Amazon Resource Name (ARN) of the change set that you want to describe.
   * @param {string} logicalResourceId - If specified, lists only the hooks related to the specified LogicalResourceId.
   * @param {string} nextToken - A string, provided by the DescribeChangeSetHooks response output, that identifies the next page of information that you want to retrieve.
   * @param {string} stackName - If you specified the name of a change set, specify the stack name or ID (ARN) of the change set you want to describe.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeChangeSetHooks(changeSetName, logicalResourceId, nextToken, stackName, callback) {
    this.describeChangeSetHooksSTSRole(changeSetName, logicalResourceId, nextToken, stackName, null, null, callback);
  }

  /**
   * @summary Returns hook-related information for the change set and a list of changes that AWS CloudFormation makes when you run the change set.
   *
   * @function describeChangeSetHooksSTSRole
   * @param {string} changeSetName - The name or Amazon Resource Name (ARN) of the change set that you want to describe.
   * @param {string} logicalResourceId - If specified, lists only the hooks related to the specified LogicalResourceId.
   * @param {string} nextToken - A string, provided by the DescribeChangeSetHooks response output, that identifies the next page of information that you want to retrieve.
   * @param {string} stackName - If you specified the name of a change set, specify the stack name or ID (ARN) of the change set you want to describe.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeChangeSetHooksSTSRole(changeSetName, logicalResourceId, nextToken, stackName, stsParams, roleName, callback) {
    const meth = 'adapter-describeChangeSetHooksSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeChangeSetHooks';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (changeSetName === undefined || changeSetName === null || changeSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['changeSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      ChangeSetName: changeSetName,
      LogicalResourceId: logicalResourceId,
      NextToken: nextToken,
      StackName: stackName,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('ChangeSet', 'describeChangeSetHooks', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeChangeSetHooksSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Activates a public third-party extension, making it available for use in stack templates.
   *
   * @function activateType
   * @param {boolean} autoUpdate - Whether to automatically update the extension in this account and Region when a new minor version is published by the extension publisher.
   * @param {string} executionRoleArn - The name of the IAM execution role to use to activate the extension.
   * @param {object} loggingConfig - Contains logging configuration information for an extension.
   * @param {number} majorVersion - The major version of this extension you want to activate, if multiple major versions are available.
   * @param {string} publicTypeArn - The Amazon Resource Name (ARN) of the public extension.
   * @param {string} publisherId - The ID of the extension publisher.
   * @param {string} type - The extension type.
   * @param {string} typeName - The name of the extension.
   * @param {string} typeNameAlias - An alias to assign to the public extension, in this account and Region.
   * @param {string} versionBump - Manually updates a previously-activated type to a new major or minor version, if available.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  activateType(autoUpdate, executionRoleArn, loggingConfig, majorVersion, publicTypeArn, publisherId, type, typeName, typeNameAlias, versionBump, callback) {
    this.activateTypeSTSRole(autoUpdate, executionRoleArn, loggingConfig, majorVersion, publicTypeArn, publisherId, type, typeName, typeNameAlias, versionBump, null, null, callback);
  }

  /**
   * @summary Activates a public third-party extension, making it available for use in stack templates.
   *
   * @function activateTypeSTSRole
   * @param {boolean} autoUpdate - Whether to automatically update the extension in this account and Region when a new minor version is published by the extension publisher.
   * @param {string} executionRoleArn - The name of the IAM execution role to use to activate the extension.
   * @param {object} loggingConfig - Contains logging configuration information for an extension.
   * @param {number} majorVersion - The major version of this extension you want to activate, if multiple major versions are available.
   * @param {string} publicTypeArn - The Amazon Resource Name (ARN) of the public extension.
   * @param {string} publisherId - The ID of the extension publisher.
   * @param {string} type - The extension type.
   * @param {string} typeName - The name of the extension.
   * @param {string} typeNameAlias - An alias to assign to the public extension, in this account and Region.
   * @param {string} versionBump - Manually updates a previously-activated type to a new major or minor version, if available.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  activateTypeSTSRole(autoUpdate, executionRoleArn, loggingConfig, majorVersion, publicTypeArn, publisherId, type, typeName, typeNameAlias, versionBump, stsParams, roleName, callback) {
    const meth = 'adapter-activateTypeSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ActivateType';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      AutoUpdate: autoUpdate,
      ExecutionRoleArn: executionRoleArn,
      LoggingConfig: loggingConfig,
      MajorVersion: majorVersion,
      PublicTypeArn: publicTypeArn,
      PublisherId: publisherId,
      Type: type,
      TypeName: typeName,
      TypeNameAlias: typeNameAlias,
      VersionBump: versionBump,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'activateType', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['activateTypeSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns configuration data for the specified CloudFormation extensions, from the CloudFormation registry for the account and Region.
   *
   * @function batchDescribeTypeConfigurations
   * @param {array} typeConfigurationIdentifiers - The list of identifiers for the desired extension configurations.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  batchDescribeTypeConfigurations(typeConfigurationIdentifiers, callback) {
    this.batchDescribeTypeConfigurationsSTSRole(typeConfigurationIdentifiers, null, null, callback);
  }

  /**
   * @summary Returns configuration data for the specified CloudFormation extensions, from the CloudFormation registry for the account and Region.
   *
   * @function batchDescribeTypeConfigurationsSTSRole
   * @param {array} typeConfigurationIdentifiers - The list of identifiers for the desired extension configurations.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  batchDescribeTypeConfigurationsSTSRole(typeConfigurationIdentifiers, stsParams, roleName, callback) {
    const meth = 'adapter-batchDescribeTypeConfigurationsSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'BatchDescribeTypeConfigurations';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (typeConfigurationIdentifiers === undefined || typeConfigurationIdentifiers === null || typeConfigurationIdentifiers === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['typeConfigurationIdentifiers'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      Action: action,
      Version: version
    };
    if (typeConfigurationIdentifiers && typeConfigurationIdentifiers.length > 0) {
      processInArray(typeConfigurationIdentifiers, queryParamsAvailable, 'TypeConfigurationIdentifiers');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'batchDescribeTypeConfigurations', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['batchDescribeTypeConfigurationsSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Deactivates a public extension that was previously activated in this account and Region.
   *
   * @function deactivateType
   * @param {string} arn - The Amazon Resource Name (ARN) for the extension, in this account and Region.
   * @param {string} type - The extension type.
   * @param {string} typeName - The type name of the extension, in this account and Region.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deactivateType(arn, type, typeName, callback) {
    this.deactivateTypeSTSRole(arn, type, typeName, null, null, callback);
  }

  /**
   * @summary Deactivates a public extension that was previously activated in this account and Region.
   *
   * @function deactivateTypeSTSRole
   * @param {string} arn - The Amazon Resource Name (ARN) for the extension, in this account and Region.
   * @param {string} type - The extension type.
   * @param {string} typeName - The type name of the extension, in this account and Region.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deactivateTypeSTSRole(arn, type, typeName, stsParams, roleName, callback) {
    const meth = 'adapter-deactivateTypeSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DeactivateType';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      Arn: arn,
      Type: type,
      TypeName: typeName,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'deactivateType', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deactivateTypeSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Publishes the specified extension to the CloudFormation registry as a public extension in this Region.
   *
   * @function publishType
   * @param {string} arn - The Amazon Resource Name (ARN) of the extension.
   * @param {string} publicVersionNumber - The version number to assign to this version of the extension.
   * @param {string} type - The type of the extension.
   * @param {string} typeName - The name of the extension.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  publishType(arn, publicVersionNumber, type, typeName, callback) {
    this.publishTypeSTSRole(arn, publicVersionNumber, type, typeName, null, null, callback);
  }

  /**
   * @summary Publishes the specified extension to the CloudFormation registry as a public extension in this Region.
   *
   * @function publishTypeSTSRole
   * @param {string} arn - The Amazon Resource Name (ARN) of the extension.
   * @param {string} publicVersionNumber - The version number to assign to this version of the extension.
   * @param {string} type - The type of the extension.
   * @param {string} typeName - The name of the extension.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  publishTypeSTSRole(arn, publicVersionNumber, type, typeName, stsParams, roleName, callback) {
    const meth = 'adapter-publishTypeSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'PublishType';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      Arn: arn,
      PublicVersionNumber: publicVersionNumber,
      Type: type,
      TypeName: typeName,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'publishType', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['publishTypeSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Specifies the configuration data for a registered CloudFormation extension, in the given account and Region.
   *
   * @function setTypeConfiguration
   * @param {string} configuration - The configuration data for the extension, in this account and Region.
   * @param {string} configurationAlias - An alias by which to refer to this extension configuration data.
   * @param {string} type - The type of the extension.
   * @param {string} typeArn - The Amazon Resource Name (ARN) for the extension, in this account and Region.
   * @param {string} typeName - The name of the extension.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setTypeConfiguration(configuration, configurationAlias, type, typeArn, typeName, callback) {
    this.setTypeConfigurationSTSRole(configuration, configurationAlias, type, typeArn, typeName, null, null, callback);
  }

  /**
   * @summary Specifies the configuration data for a registered CloudFormation extension, in the given account and Region.
   *
   * @function setTypeConfigurationSTSRole
   * @param {string} configuration - The configuration data for the extension, in this account and Region.
   * @param {string} configurationAlias - An alias by which to refer to this extension configuration data.
   * @param {string} type - The type of the extension.
   * @param {string} typeArn - The Amazon Resource Name (ARN) for the extension, in this account and Region.
   * @param {string} typeName - The name of the extension.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setTypeConfigurationSTSRole(configuration, configurationAlias, type, typeArn, typeName, stsParams, roleName, callback) {
    const meth = 'adapter-setTypeConfigurationSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'SetTypeConfiguration';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (configuration === undefined || configuration === null || configuration === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['configuration'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      Configuration: configuration,
      ConfigurationAlias: configurationAlias,
      Type: type,
      TypeArn: typeArn,
      TypeName: typeName,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'setTypeConfiguration', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['setTypeConfigurationSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Tests a registered extension to make sure it meets all necessary requirements for being published in the CloudFormation registry.

   *
   * @function testType
   * @param {string} arn - The Amazon Resource Name (ARN) of the extension.
   * @param {string} logDeliveryBucket - The S3 bucket to which CloudFormation delivers the contract test execution logs.
   * @param {string} type - The type of the extension.
   * @param {string} typeName - The name of the extension.
   * @param {string} versionId - The version of the extension to test.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  testType(arn, logDeliveryBucket, type, typeName, versionId, callback) {
    this.testTypeSTSRole(arn, logDeliveryBucket, type, typeName, versionId, null, null, callback);
  }

  /**
   * @summary Tests a registered extension to make sure it meets all necessary requirements for being published in the CloudFormation registry.

   *
   * @function testTypeSTSRole
   * @param {string} arn - The Amazon Resource Name (ARN) of the extension.
   * @param {string} logDeliveryBucket - The S3 bucket to which CloudFormation delivers the contract test execution logs.
   * @param {string} type - The type of the extension.
   * @param {string} typeName - The name of the extension.
   * @param {string} versionId - The version of the extension to test.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  testTypeSTSRole(arn, logDeliveryBucket, type, typeName, versionId, stsParams, roleName, callback) {
    const meth = 'adapter-testTypeSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'TestType';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      Arn: arn,
      LogDeliveryBucket: logDeliveryBucket,
      Type: type,
      TypeName: typeName,
      VersionId: versionId,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Type', 'testType', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['testTypeSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Activate trusted access with AWS Organizations.
   *
   * @function activateOrganizationsAccess
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  activateOrganizationsAccess(callback) {
    this.activateOrganizationsAccessSTSRole(null, null, callback);
  }

  /**
   * @summary Activate trusted access with AWS Organizations.
   *
   * @function activateOrganizationsAccessSTSRole
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  activateOrganizationsAccessSTSRole(stsParams, roleName, callback) {
    const meth = 'adapter-activateOrganizationsAccessSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ActivateOrganizationsAccess';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      Action: action
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Organization', 'activateOrganizationsAccess', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['activateOrganizationsAccessSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Deactivates trusted access with AWS Organizations.
   *
   * @function deactivateOrganizationsAccess
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deactivateOrganizationsAccess(callback) {
    this.deactivateOrganizationsAccessSTSRole(null, null, callback);
  }

  /**
   * @summary Deactivates trusted access with AWS Organizations.
   *
   * @function deactivateOrganizationsAccessSTSRole
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deactivateOrganizationsAccessSTSRole(stsParams, roleName, callback) {
    const meth = 'adapter-deactivateOrganizationsAccessSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DeactivateOrganizationsAccess';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      Action: action
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Organization', 'deactivateOrganizationsAccess', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['deactivateOrganizationsAccessSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Retrieves information about the account's OrganizationAccess status.
   *
   * @function describeOrganizationsAccess
   * @param {string} callAs - Specifies whether you are acting as an account administrator in the organization's management account or as a delegated administrator in a member account.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeOrganizationsAccess(callAs, callback) {
    this.describeOrganizationsAccessSTSRole(callAs, null, null, callback);
  }

  /**
   * @summary Retrieves information about the account's OrganizationAccess status.
   *
   * @function describeOrganizationsAccessSTSRole
   * @param {string} callAs - Specifies whether you are acting as an account administrator in the organization's management account or as a delegated administrator in a member account.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describeOrganizationsAccessSTSRole(callAs, stsParams, roleName, callback) {
    const meth = 'adapter-describeOrganizationsAccessSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribeOrganizationsAccess';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      CallAs: callAs,
      Action: action
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Organization', 'describeOrganizationsAccess', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describeOrganizationsAccessSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Import existing stacks into a new stack sets.
   *
   * @function importStacksToStackSet
   * @param {string} callAs - By default, SELF is specified. Use SELF for stack sets with self-managed permissions.
   * @param {string} operationId - A unique, user defined, identifier for the stack set operation.
   * @param {object} operationPreferences - The user-specified preferences for how AWS CloudFormation performs a stack set operation.
   * @param {array} organizationalUnitIds - The list of OU ID's to which the stacks being imported has to be mapped as deployment target.
   * @param {array} stackIds - The IDs of the stacks you are importing into a stack set. You import up to 10 stacks per stack set at a time.
   * @param {string} stackIdsUrl - The Amazon S3 URL which contains list of stack ids to be inputted.
   *  @param {string} stackSetName - The name of the stack set.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  importStacksToStackSet(callAs, operationId, operationPreferences, organizationalUnitIds, stackIds, stackIdsUrl, stackSetName, callback) {
    this.importStacksToStackSetSTSRole(callAs, operationId, operationPreferences, organizationalUnitIds, stackIds, stackIdsUrl, stackSetName, null, null, callback);
  }

  /**
   * @summary Import existing stacks into a new stack sets.
   *
   * @function importStacksToStackSetSTSRole
   * @param {string} callAs - By default, SELF is specified. Use SELF for stack sets with self-managed permissions.
   * @param {string} operationId - A unique, user defined, identifier for the stack set operation.
   * @param {object} operationPreferences - The user-specified preferences for how AWS CloudFormation performs a stack set operation.
   * @param {array} organizationalUnitIds - The list of OU ID's to which the stacks being imported has to be mapped as deployment target.
   * @param {array} stackIds - The IDs of the stacks you are importing into a stack set. You import up to 10 stacks per stack set at a time.
   * @param {string} stackIdsUrl - The Amazon S3 URL which contains list of stack ids to be inputted.
   *  @param {string} stackSetName - The name of the stack set.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  importStacksToStackSetSTSRole(callAs, operationId, operationPreferences, organizationalUnitIds, stackIds, stackIdsUrl, stackSetName, stsParams, roleName, callback) {
    const meth = 'adapter-importStacksToStackSetSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'ImportStacksToStackSet';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackSetName === undefined || stackSetName === null || stackSetName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackSetName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      CallAs: callAs,
      OperationId: operationId,
      OperationPreferences: operationPreferences,
      StackIdsUrl: stackIdsUrl,
      StackSetName: stackSetName,
      Action: action,
      Version: version
    };
    if (organizationalUnitIds && organizationalUnitIds.length > 0) {
      processInArray(organizationalUnitIds, queryParamsAvailable, 'OrganizationalUnitIds');
    }

    if (stackIds && stackIds.length > 0) {
      processInArray(stackIds, queryParamsAvailable, 'StackIds');
    }

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'importStacksToStackSet', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['importStacksToStackSetSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary When specifying RollbackStack, you preserve the state of previously provisioned resources when an operation fails.
   *
   * @function rollbackStack
   * @param {string} clientRequestToken - A unique identifier for this RollbackStack request.
   * @param {string} roleARN - The Amazon Resource Name (ARN) of an AWS Identity and Access Management role that AWS CloudFormation assumes to rollback the stack.
   * @param {string} stackName - The name that's associated with the stack.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  rollbackStack(clientRequestToken, roleARN, stackName, callback) {
    this.rollbackStackSTSRole(clientRequestToken, roleARN, stackName, null, null, callback);
  }

  /**
   * @summary When specifying RollbackStack, you preserve the state of previously provisioned resources when an operation fails.
   *
   * @function rollbackStackSTSRole
   * @param {string} clientRequestToken - A unique identifier for this RollbackStack request.
   * @param {string} roleARN - The Amazon Resource Name (ARN) of an AWS Identity and Access Management role that AWS CloudFormation assumes to rollback the stack.
   * @param {string} stackName - The name that's associated with the stack.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  rollbackStackSTSRole(clientRequestToken, roleARN, stackName, stsParams, roleName, callback) {
    const meth = 'adapter-rollbackStackSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'RollbackStack';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (stackName === undefined || stackName === null || stackName === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['stackName'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      ClientRequestToken: clientRequestToken,
      RoleARN: roleARN,
      StackName: stackName,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Stack', 'rollbackStack', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['rollbackStackSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Registers your account as a publisher of public extensions in the CloudFormation registry.
   *
   * @function registerPublisher
   * @param {boolean} acceptTermsAndConditions - Whether you accept the Terms and Conditions for publishing extensions in the CloudFormation registry.
   * @param {string} connectionArn - If you are using a Bitbucket or GitHub account for identity verification, the Amazon Resource Name (ARN) for your connection to that account.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  registerPublisher(acceptTermsAndConditions, connectionArn, callback) {
    this.registerPublisherSTSRole(acceptTermsAndConditions, connectionArn, null, null, callback);
  }

  /**
   * @summary Registers your account as a publisher of public extensions in the CloudFormation registry.
   *
   * @function registerPublisherSTSRole
   * @param {boolean} acceptTermsAndConditions - Whether you accept the Terms and Conditions for publishing extensions in the CloudFormation registry.
   * @param {string} connectionArn - If you are using a Bitbucket or GitHub account for identity verification, the Amazon Resource Name (ARN) for your connection to that account.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  registerPublisherSTSRole(acceptTermsAndConditions, connectionArn, stsParams, roleName, callback) {
    const meth = 'adapter-registerPublisherSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'RegisterPublisher';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      AcceptTermsAndConditions: acceptTermsAndConditions,
      ConnectionArn: connectionArn,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Account', 'registerPublisher', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['registerPublisherSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Returns information about a CloudFormation extension publisher.
   *
   * @function describePublisher
   * @param {string} publisherId - The ID of the extension publisher.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describePublisher(publisherId, callback) {
    this.describePublisherSTSRole(publisherId, null, null, callback);
  }

  /**
   * @summary Returns information about a CloudFormation extension publisher.
   *
   * @function describePublisherSTSRole
   * @param {string} publisherId - The ID of the extension publisher.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  describePublisherSTSRole(publisherId, stsParams, roleName, callback) {
    const meth = 'adapter-describePublisherSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'DescribePublisher';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      PublisherId: publisherId,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Account', 'describePublisher', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['describePublisherSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Reports progress of a resource handler to CloudFormation.
   *
   * @function recordHandlerProgress
   * @param {string} bearerToken - Reserved for use by the CloudFormation CLI.
   * @param {string} clientRequestToken - Reserved for use by the CloudFormation CLI.
   * @param {string} currentOperationStatus - Reserved for use by the CloudFormation CLI.
   * @param {string} errorCode - Reserved for use by the CloudFormation CLI.
   * @param {string} operationStatus - Reserved for use by the CloudFormation CLI.
   * @param {string} resourceModel - Reserved for use by the CloudFormation CLI.
   * @param {string} statusMessage - Reserved for use by the CloudFormation CLI.
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  recordHandlerProgress(bearerToken, clientRequestToken, currentOperationStatus, errorCode, operationStatus, resourceModel, statusMessage, callback) {
    this.recordHandlerProgressSTSRole(bearerToken, clientRequestToken, currentOperationStatus, errorCode, operationStatus, resourceModel, statusMessage, null, null, callback);
  }

  /**
   * @summary Reports progress of a resource handler to CloudFormation.
   *
   * @function recordHandlerProgressSTSRole
   * @param {string} bearerToken - Reserved for use by the CloudFormation CLI.
   * @param {string} clientRequestToken - Reserved for use by the CloudFormation CLI.
   * @param {string} currentOperationStatus - Reserved for use by the CloudFormation CLI.
   * @param {string} errorCode - Reserved for use by the CloudFormation CLI.
   * @param {string} operationStatus - Reserved for use by the CloudFormation CLI.
   * @param {string} resourceModel - Reserved for use by the CloudFormation CLI.
   * @param {string} statusMessage - Reserved for use by the CloudFormation CLI.
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  recordHandlerProgressSTSRole(bearerToken, clientRequestToken, currentOperationStatus, errorCode, operationStatus, resourceModel, statusMessage, stsParams, roleName, callback) {
    const meth = 'adapter-recordHandlerProgressSTSRole';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    const action = 'RecordHandlerProgress';
    const version = '2010-05-15';

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (bearerToken === undefined || bearerToken === null || bearerToken === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['bearerToken'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (operationStatus === undefined || operationStatus === null || operationStatus === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['operationStatus'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {
      BearerToken: bearerToken,
      ClientRequestToken: clientRequestToken,
      CurrentOperationStatus: currentOperationStatus,
      ErrorCode: errorCode,
      OperationStatus: operationStatus,
      ResourceModel: resourceModel,
      StatusMessage: statusMessage,
      Action: action,
      Version: version
    };

    const queryParams = {};
    const pathVars = [];
    const bodyVars = {};

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // if you want to expose addlHeaders to workflow, add it to the method signature here and in pronghorn.json
    let thisHeaderData = null;
    // if the additional headers was passed in as a string parse the json into an object
    if (thisHeaderData !== null && thisHeaderData.constructor === String) {
      try {
        // parse the additional headers object that was passed in
        thisHeaderData = JSON.parse(thisHeaderData);
      } catch (err) {
        const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'addlHeaders string must be a stringified JSON', [], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
    }

    let callProperties = null;
    if (stsParams && stsParams.region) {
      callProperties = {};
      callProperties.region = stsParams.region;
      callProperties.host = `${this.allProps.service}.${stsParams.region}.amazonaws.com`;
      delete stsParams.region;
      if (Object.keys(stsParams).length === 0) {
        stsParams = null;
      }
    }

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams,
      addlHeaders: thisHeaderData,
      authData: {
        stsParams,
        roleName
      },
      callProperties
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('Resource', 'recordHandlerProgress', reqObj, false, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['recordHandlerProgressSTSRole'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }
}

module.exports = AwsCloudFormation;
