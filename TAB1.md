# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the AwsCloudFormation System. The API that was used to build the adapter for AwsCloudFormation is usually available in the report directory of this adapter. The adapter utilizes the AwsCloudFormation API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The AWS CloudFormation adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS CloudFormation.

With this adapter you have the ability to perform operations such as:

- Get Template
- Validate Template
- Create Stack
- Get Create Stack

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
