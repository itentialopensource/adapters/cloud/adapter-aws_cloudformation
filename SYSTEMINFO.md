# AWS CloudFormation

Vendor: Amazon Web Services (AWS)
Homepage: https://aws.amazon.com/

Product: CloudFormation
Product Page: https://aws.amazon.com/cloudformation/

## Introduction
We classify AWS CloudFormation into the Cloud domain as it provides a cloud provisioning solution using infrastructure as code. 

"AWS CloudFormation lets you model, provision, and manage AWS and third-party resources by treating infrastructure as code."

## Why Integrate
The AWS CloudFormation adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS CloudFormation.

With this adapter you have the ability to perform operations such as:

- Get Template
- Validate Template
- Create Stack
- Get Create Stack

## Additional Product Documentation
The [API documents for AWS CloudFormation](https://docs.aws.amazon.com/AWSCloudFormation/latest/APIReference/Welcome.html)
