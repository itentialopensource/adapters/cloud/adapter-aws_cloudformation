
## 0.2.1 [11-20-2023]

* Revert "Merge branch 'minor/ADAPT-2792' into 'master'"

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!6

---

## 0.2.0 [08-11-2023]

* Add missing endpoints

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!3

---

## 0.1.4 [03-01-2021]

* migrate to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!2

---

## 0.1.3 [07-06-2020]

* Migration

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!1

---

## 0.1.2 [07-05-2020]

* Migration

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!1

---

## 0.1.1 [02-25-2020]

* Bug fixes and performance improvements

See commit f7b7fbc

---
