
## 0.3.10 [11-11-2024]

* more auth changes

See merge request itentialopensource/adapters/adapter-aws_cloudformation!26

---

## 0.3.9 [10-15-2024]

* Changes made at 2024.10.14_21:12PM

See merge request itentialopensource/adapters/adapter-aws_cloudformation!25

---

## 0.3.8 [09-30-2024]

* update auth docs

See merge request itentialopensource/adapters/adapter-aws_cloudformation!23

---

## 0.3.7 [09-12-2024]

* add properties for sts

See merge request itentialopensource/adapters/adapter-aws_cloudformation!22

---

## 0.3.6 [08-23-2024]

* turn off savemetric

See merge request itentialopensource/adapters/adapter-aws_cloudformation!21

---

## 0.3.5 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-aws_cloudformation!20

---

## 0.3.4 [08-14-2024]

* Changes made at 2024.08.14_19:25PM

See merge request itentialopensource/adapters/adapter-aws_cloudformation!19

---

## 0.3.3 [08-07-2024]

* changes for adding member

See merge request itentialopensource/adapters/adapter-aws_cloudformation!18

---

## 0.3.2 [08-07-2024]

* Changes made at 2024.08.06_21:08PM

See merge request itentialopensource/adapters/adapter-aws_cloudformation!17

---

## 0.3.1 [08-06-2024]

* Changes made at 2024.08.06_15:31PM

See merge request itentialopensource/adapters/adapter-aws_cloudformation!16

---

## 0.3.0 [07-08-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!15

---

## 0.2.10 [03-28-2024]

* Changes made at 2024.03.28_13:26PM

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!14

---

## 0.2.9 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!13

---

## 0.2.8 [03-13-2024]

* Changes made at 2024.03.13_11:46AM

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!12

---

## 0.2.7 [03-11-2024]

* Changes made at 2024.03.11_15:37PM

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!11

---

## 0.2.6 [02-28-2024]

* Changes made at 2024.02.28_11:53AM

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!10

---

## 0.2.5 [01-27-2024]

* Added dynamic region support to sts param

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!9

---

## 0.2.4 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!8

---

## 0.2.3 [12-14-2023]

* Patch/update package

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!7

---

## 0.2.2 [11-21-2023]

* Patch/update package

See merge request itentialopensource/adapters/cloud/adapter-aws_cloudformation!7

---
